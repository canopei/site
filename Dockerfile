FROM alpine:latest
MAINTAINER Aris Buzachis <aris@canopei.com>

RUN apk add --update --no-cache ca-certificates

RUN mkdir -p /var/sng/bin
COPY build/ /var/sng/bin/

RUN touch crontab.tmp \
    && echo '0 0 * * * cd /var/sng/bin; ./runreindex' > crontab.tmp \
    && crontab crontab.tmp \
    && rm -rf crontab.tmp


EXPOSE 8080
EXPOSE 8180

CMD crond && cd /var/sng/bin; ./server & ./api & ./sync & ./reindex & tail -f /dev/null