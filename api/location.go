package main

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	accountPb "bitbucket.org/canopei/account/protobuf"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// LocationRequestsHandler handles location related requests
type LocationRequestsHandler struct {
	Logger        *logrus.Entry
	AccountClient accountPb.AccountServiceClient
	SelfClient    sitePb.SiteServiceClient
}

// NewLocationRequestHandler creates a LocationRequestsHandler
func NewLocationRequestHandler(logger *logrus.Entry, accountClient accountPb.AccountServiceClient, selfClient sitePb.SiteServiceClient) *LocationRequestsHandler {
	return &LocationRequestsHandler{
		Logger:        logger,
		AccountClient: accountClient,
		SelfClient:    selfClient,
	}
}

// RegisterLocationRoutes registers the routes to a mux router
func (s *LocationRequestsHandler) RegisterLocationRoutes(r *mux.Router) {
	r.HandleFunc("/v1/locations/{location_uuid}/favourite/{account_uuid}", s.HandleCreateLocationFavouriteRequest).Methods("POST")
	r.HandleFunc("/v1/locations/{location_uuid}/favourite/{account_uuid}", s.HandleDeleteLocationFavouriteRequest).Methods("DELETE")
	r.HandleFunc("/v1/locations/favourite/{account_uuid}", s.HandleGetLocationFavouriteForAccountRequest).Methods("GET")
}

// HandleCreateLocationFavouriteRequest handles a request for creating a staff favourite
func (s *LocationRequestsHandler) HandleCreateLocationFavouriteRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	locationUUID := vars["location_uuid"]
	accountUUID := vars["account_uuid"]

	logger.Infof("HandleCreateLocationFavouriteRequest account '%s', location '%s'", accountUUID, locationUUID)
	defer timeTrack(logger, time.Now(), "HandleCreateLocationFavouriteRequest")

	ctx := context.Background()

	account, err := s.AccountClient.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	response, err := s.SelfClient.GetLocations(ctx, &sitePb.GetLocationsRequest{Uuids: locationUUID})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}
	if len(response.Locations) == 0 {
		res.WriteHeader(http.StatusNotFound)
		return
	}
	location := response.Locations[0]

	_, err = s.SelfClient.CreateLocationFavourite(ctx, &sitePb.LocationFavouriteRequest{AccountId: account.Id, LocationId: location.Id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	res.WriteHeader(http.StatusNoContent)
}

// HandleDeleteLocationFavouriteRequest handles a request for removing a location favourite
func (s *LocationRequestsHandler) HandleDeleteLocationFavouriteRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	locationUUID := vars["location_uuid"]
	accountUUID := vars["account_uuid"]

	logger.Infof("HandleDeleteLocationFavouriteRequest account '%s', location '%s'", accountUUID, locationUUID)
	defer timeTrack(logger, time.Now(), "HandleDeleteLocationFavouriteRequest")

	ctx := context.Background()

	account, err := s.AccountClient.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	response, err := s.SelfClient.GetLocations(ctx, &sitePb.GetLocationsRequest{Uuids: locationUUID})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}
	if len(response.Locations) == 0 {
		res.WriteHeader(http.StatusNotFound)
		return
	}
	location := response.Locations[0]

	_, err = s.SelfClient.DeleteLocationFavourite(ctx, &sitePb.LocationFavouriteRequest{AccountId: account.Id, LocationId: location.Id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	res.WriteHeader(http.StatusNoContent)
}

// HandleGetLocationFavouriteForAccountRequest fetches the favourite location UUIDs for an account
func (s *LocationRequestsHandler) HandleGetLocationFavouriteForAccountRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]

	logger.Infof("HandleGetLocationFavouriteForAccountRequest account '%s'", accountUUID)
	defer timeTrack(logger, time.Now(), "HandleGetLocationFavouriteForAccountRequest")

	ctx := context.Background()

	account, err := s.AccountClient.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	response, err := s.SelfClient.GetLocationFavouriteForAccount(ctx, &sitePb.GetLocationFavouriteForAccountRequest{AccountId: account.Id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}
	if response.LocationUuids == nil {
		response.LocationUuids = []string{}
	}

	res.WriteHeader(http.StatusOK)
	s.writeJSONResponse(res, response.LocationUuids)
}

func (s *LocationRequestsHandler) writeJSONResponse(res http.ResponseWriter, response interface{}) error {
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		s.Logger.Error("Failed to Marshal the response.")
		res.WriteHeader(http.StatusInternalServerError)
		return err
	}

	res.Write([]byte(jsonResponse))
	return nil
}
