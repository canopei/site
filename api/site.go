package main

import (
	"encoding/json"
	"net/http"
	"time"

	accountPb "bitbucket.org/canopei/account/protobuf"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	siteMb "bitbucket.org/canopei/mindbody/services/site"
	"bitbucket.org/canopei/site"
	"bitbucket.org/canopei/site/config"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

// SiteRequestsHandler handles sites related requests
type SiteRequestsHandler struct {
	Logger         *logrus.Entry
	AccountClient  accountPb.AccountServiceClient
	SelfClient     sitePb.SiteServiceClient
	SiteMBClient   *siteMb.Site_x0020_ServiceSoap
	MindbodyConfig *config.MindbodyConfig
}

// NewSiteRequestHandler creates a SiteRequestsHandler
func NewSiteRequestHandler(
	logger *logrus.Entry,
	accountClient accountPb.AccountServiceClient,
	selfClient sitePb.SiteServiceClient,
	siteMBClient *siteMb.Site_x0020_ServiceSoap,
	mindbodyConfig *config.MindbodyConfig,
) *SiteRequestsHandler {
	return &SiteRequestsHandler{
		Logger:         logger,
		AccountClient:  accountClient,
		SelfClient:     selfClient,
		SiteMBClient:   siteMBClient,
		MindbodyConfig: mindbodyConfig,
	}
}

// RegisterSiteRoutes registers the routes to a mux router
func (s *SiteRequestsHandler) RegisterSiteRoutes(r *mux.Router) {
	r.HandleFunc("/v1/sites/mb/", s.HandleGetAllActivatedSitesRequest).Methods("GET")
}

// HandleGetAllActivatedSitesRequest handles a request for creating a staff favourite
func (s *SiteRequestsHandler) HandleGetAllActivatedSitesRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	logger.Infof("HandleGetAllActivatedSitesRequest")
	defer timeTrack(logger, time.Now(), "HandleGetAllActivatedSitesRequest")

	// Sync the clients page by page
	mbRequest := mbHelpers.CreateSiteMBRequest([]int32{0}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
	mbGetSitesResponse, err := s.SiteMBClient.GetSites(&siteMb.GetSites{Request: &siteMb.GetSitesRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		s.Logger.Errorf("Cannot fetch the list of sites from Mindbody: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	mbGetSitesResult := mbGetSitesResponse.GetSitesResult
	if mbGetSitesResult.ErrorCode.Int != 200 {
		s.Logger.Errorf("Mindbody API response error: %v", mbGetSitesResult.Message)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	s.Logger.Infof("Got %d sites from Mindbody.", len(mbGetSitesResult.Sites.Site))

	response := []*sitePb.Site{}

	if len(mbGetSitesResult.Sites.Site) > 0 {
		for _, mbSite := range mbGetSitesResult.Sites.Site {
			response = append(response, site.MbSiteToPbSite(mbSite))
		}
	}

	res.WriteHeader(http.StatusOK)
	s.writeJSONResponse(res, response)
}

func (s *SiteRequestsHandler) writeJSONResponse(res http.ResponseWriter, response interface{}) error {
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		s.Logger.Error("Failed to Marshal the response.")
		res.WriteHeader(http.StatusInternalServerError)
		return err
	}

	res.Write([]byte(jsonResponse))
	return nil
}
