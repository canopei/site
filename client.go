package site

import (
	"fmt"

	sitePb "bitbucket.org/canopei/site/protobuf"
	"google.golang.org/grpc"
)

// NewClient returns a gRPC client for interacting with the site service.
func NewClient(addr string) (sitePb.SiteServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return sitePb.NewSiteServiceClient(conn), conn.Close, nil
}
