package config

import (
	"bitbucket.org/canopei/golibs/auth"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/logging"
	"github.com/BurntSushi/toml"
)

// ServiceConfig holds the service general configuration
type ServiceConfig struct {
	Name       string
	Env        string
	ApiPort    int16 `toml:"api_port"`
	GrpcPort   int16 `toml:"grpc_port"`
	HealthPort int16 `toml:"health_port"`
}

// DatabaseConfig holds the database specific configuration
type DatabaseConfig struct {
	Addr     string
	Username string
	Password string
	DbName   string `toml:"dbname"`
}

// MindbodyConfig holds the Mindbody configuration
type MindbodyConfig struct {
	SourceName     string `toml:"source_name"`
	SourcePassword string `toml:"source_password"`
}

// QueueConfig holds the configuration for the queue service
type QueueConfig struct {
	Addr         string
	SyncTopic    string `toml:"sync_topic"`
	ReindexTopic string `toml:"reindex_topic"`
}

// AccountServiceConfig holds the configuration for the Account service
type AccountServiceConfig struct {
	Addr string
}

// SiteServiceConfig holds the configuration for the external Site service
type SiteServiceConfig struct {
	Addr string
}

// SyncConfig holds the configuration for the sync process
type SyncConfig struct {
	Workers               int    `toml:"workers"`
	MaxBatchSize          int    `toml:"max_batch_size"`
	BatchInterval         int    `toml:"batch_interval"`
	MaxInFlight           int    `toml:"max_in_flight"`
	ReconnectInterval     int    `toml:"reconnect_interval"`
	SitesSyncInterval     int    `toml:"sites_sync_interval"`
	FullSitesSyncCronTime string `toml:"full_sites_sync_cron_time"`
	ReindexTimeout        int    `toml:"reindex_timeout"`
}

// Config holds the entire configuration
type Config struct {
	Service        ServiceConfig          `toml:"service"`
	Db             DatabaseConfig         `toml:"database"`
	Logstash       logging.LogstashConfig `toml:"logstash"`
	Mindbody       MindbodyConfig         `toml:"mindbody"`
	Queue          QueueConfig            `toml:"queue"`
	Sync           SyncConfig             `toml:"sync"`
	Elasticsearch  elasticsearch.Config   `toml:"elasticsearch"`
	Auth           auth.Config            `toml:"auth"`
	AccountService AccountServiceConfig   `toml:"account_service"`
	SiteService    SiteServiceConfig      `toml:"site_service"`
}

// LoadConfig loads the configuration from the given filepath
func LoadConfig(filePath string) (*Config, error) {
	var conf Config
	_, err := toml.DecodeFile(filePath, &conf)

	return &conf, err
}
