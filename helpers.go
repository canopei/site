package site

import (
	"context"
	"fmt"
	"strings"

	elastic "gopkg.in/olivere/elastic.v5"

	"bitbucket.org/canopei/golibs/elasticsearch"
	siteMb "bitbucket.org/canopei/mindbody/services/site"
	sitePb "bitbucket.org/canopei/site/protobuf"
)

const sqlDatetimeFormat = "2006-01-02 15:04:05"

type ReindexProgress map[string]map[string]bool

func findSiteByMBID(sites []*sitePb.Site, mbID int32) (*sitePb.Site, error) {
	for _, site := range sites {
		if site.MbId == mbID {
			return site, nil
		}
	}
	return nil, fmt.Errorf("Site was not found")
}

// We monitor only the fields we want to sync from MB
func hasLocationChanged(dbLocation *sitePb.Location, mbLocation *sitePb.Location) bool {
	return dbLocation.HasClasses != mbLocation.HasClasses
}

func MbSiteToPbSite(mbSite *siteMb.Site) *sitePb.Site {
	return &sitePb.Site{
		MbId:         mbSite.ID.Int,
		Name:         strings.TrimSpace(mbSite.Name),
		Description:  strings.TrimSpace(mbSite.Description),
		ContactEmail: strings.TrimSpace(mbSite.ContactEmail),
	}
}

func mbLocationToPbLocation(mbLocation *siteMb.Location) *sitePb.Location {
	return &sitePb.Location{
		MbId:        mbLocation.ID.Int,
		Name:        mbLocation.Name,
		Description: mbLocation.Description,
		Phone:       mbLocation.Phone,
		City:        mbLocation.City,
		Address:     mbLocation.Address,
		Address2:    mbLocation.Address2,
		PostalCode:  mbLocation.PostalCode,
		HasClasses:  mbLocation.HasClasses,
		Latitude:    mbLocation.Latitude.Float,
		Longitude:   mbLocation.Longitude.Float,
	}
}

func indexSiteToES(esService *elasticsearch.Service, indexName string, dbSite *sitePb.Site) (*elastic.IndexResponse, error) {
	return esService.Client.
		Index().
		Index(indexName).
		Type("site").
		Id(dbSite.Uuid).
		BodyJson(GetESSite(dbSite)).
		Do(context.Background())
}

func deleteSiteFromES(esService *elasticsearch.Service, indexName string, dbSiteUUID string) (*elastic.DeleteResponse, error) {
	return esService.Client.
		Delete().
		Index(indexName).
		Type("site").
		Id(dbSiteUUID).
		Do(context.Background())
}

func indexLocationToES(esService *elasticsearch.Service, indexName string, parentSiteUUID string, dbLocation *sitePb.Location) (*elastic.IndexResponse, error) {
	return esService.Client.
		Index().
		Index(indexName).
		Type("location").
		Parent(parentSiteUUID).
		Id(dbLocation.Uuid).
		BodyJson(GetESLocation(dbLocation)).
		Do(context.Background())
}

func deleteLocationFromES(esService *elasticsearch.Service, indexName string, parentSiteUUID string, dbLocationUUID string) (*elastic.DeleteResponse, error) {
	return esService.Client.
		Delete().
		Index(indexName).
		Type("location").
		Parent(parentSiteUUID).
		Id(dbLocationUUID).
		Do(context.Background())
}
