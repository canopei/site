package site

import (
	"testing"

	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/stretchr/testify/assert"
)

// Just a simple sanity check
func TestHasLocationChanged(t *testing.T) {
	assert := assert.New(t)

	dbLocation := &sitePb.Location{HasClasses: true}
	mbLocation := &sitePb.Location{HasClasses: false}
	assert.True(hasLocationChanged(dbLocation, mbLocation))

	dbLocation = &sitePb.Location{HasClasses: true}
	mbLocation = &sitePb.Location{HasClasses: true}
	assert.False(hasLocationChanged(dbLocation, mbLocation))
}
