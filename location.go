package site

import (
	sitePb "bitbucket.org/canopei/site/protobuf"
)

// ESLocation is the public representation of the Location type
type ESLocation struct {
	UUID        string  `json:"uuid"`
	Name        string  `json:"locationName"`
	Description string  `json:"locationDescription"`
	City        string  `json:"locationCity"`
	CityNA      string  `json:"cityNA"`
	Address     string  `json:"locationAddress"`
	Address2    string  `json:"locationAddress2"`
	PostalCode  string  `json:"postalCode"`
	HasClasses  bool    `json:"hasClasses"`
	Featured    int32   `json:"featured"`
	Trending    int32   `json:"trending"`
	Latitude    float64 `json:"latitude"`
	Longitude   float64 `json:"longitude"`
	CreatedAt   string  `json:"createdAt"`
}

// GetESLocation creates a ESLocation from a Location object
func GetESLocation(location *sitePb.Location) *ESLocation {
	return &ESLocation{
		UUID:        location.Uuid,
		Name:        location.Name,
		Description: location.Description,
		City:        location.City,
		CityNA:      location.CityGroupName,
		Address:     location.Address,
		Address2:    location.Address2,
		PostalCode:  location.PostalCode,
		HasClasses:  location.HasClasses,
		Featured:    location.Featured,
		Trending:    location.Trending,
		Latitude:    location.Latitude,
		Longitude:   location.Longitude,
		CreatedAt:   location.CreatedAt,
	}
}
