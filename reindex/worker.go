package main

import (
	"strings"
	"time"

	"encoding/json"

	"sync"

	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/logging"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	"bitbucket.org/canopei/golibs/slices"
	"bitbucket.org/canopei/site"
	"bitbucket.org/canopei/site/config"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

// Worker is a queue consumer worker implementation
type Worker struct {
	ID                   int
	Config               *config.SyncConfig
	Logger               *logrus.Entry
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	ReindexService       *site.ReindexService
	SiteService          sitePb.SiteServiceClient

	Buffers map[string]chan ChanEntry
	Locks   map[string]*sync.Mutex
}

// ChanEntry represents a channel entry containing the original NSQ message and a sync message
type ChanEntry struct {
	NSQMessage     *nsq.Message
	ReindexMessage *queueHelper.ReindexMessage
}

// NewWorker creates a new Worker instance
func NewWorker(
	id int,
	config *config.Config,
	logger *logrus.Entry,
	siteService sitePb.SiteServiceClient,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
) *Worker {
	workerLogger := logging.CloneLogrusEntry(logger)
	workerLogger.Data["workerID"] = id

	// Get a new SyncService
	reindexService := site.NewReindexService(workerLogger, queue, elasticsearchService, siteService)

	return &Worker{
		ID:                   id,
		Config:               &config.Sync,
		Logger:               workerLogger,
		SiteService:          siteService,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		ReindexService:       reindexService,
	}
}

// HandleMessage handles any queue message
func (w *Worker) HandleMessage(message *nsq.Message) error {
	// Do not auto-finish or requeue messages
	message.DisableAutoResponse()

	// If we cannot unmarshal it into a generic queue message, then drop it
	reindexMessage := &queueHelper.ReindexMessage{}
	err := json.Unmarshal(message.Body, reindexMessage)
	if err != nil {
		w.Logger.Warningf("Unable to unmarshal message: %v", message)
		message.Finish()
		return nil
	}

	if reindexMessage.Status == queueHelper.ReindexStatusStart {
		// Push the message to the according channel
		switch reindexMessage.ObjectType {
		case queueHelper.ObjectTypeSite:
			fallthrough
		case queueHelper.ObjectTypeSiteLocations:
			w.Buffers[string(reindexMessage.ObjectType)] <- ChanEntry{NSQMessage: message, ReindexMessage: reindexMessage}
		default:
			// If we don't recognize the type, warning and drop the message.
			w.Logger.Warningf("Skipping message with type '%s'.", reindexMessage.ObjectType)
			message.Finish()
		}
	} else {
		message.Finish()
	}

	return nil
}

// Start makes a worker process the channels
func (w *Worker) Start() {
	// Initialize the queues and the map of mutexes
	w.Locks = map[string]*sync.Mutex{}
	w.Buffers = map[string]chan ChanEntry{}

	channels := map[string]func([]ChanEntry){
		"site":           w.ProcessSites,
		"site_locations": w.ProcessSiteLocations,
	}

	for chName, chProcessor := range channels {
		w.startQueueProcessing(chName, chProcessor)
	}
}

func (w *Worker) startQueueProcessing(name string, process func([]ChanEntry)) {
	// Initialize
	w.Locks[name] = &sync.Mutex{}
	w.Buffers[name] = make(chan ChanEntry, w.Config.MaxBatchSize*3)

	go func() {
		batch := []ChanEntry{}
		ticker := time.NewTicker(time.Duration(w.Config.BatchInterval) * time.Second)
		tickerC := ticker.C

		for {
			// If we hit the batch limit or the ticker ticks, process the batch
			select {
			case f := <-w.Buffers[name]:
				batch = append(batch, f)
				if len(batch) >= w.Config.MaxBatchSize {
					process(batch)

					w.Locks[name].Lock()
					batch = []ChanEntry{}
					w.Locks[name].Unlock()
				}

				// Reset the ticker
				ticker = time.NewTicker(time.Duration(w.Config.BatchInterval) * time.Second)
				tickerC = ticker.C
			case <-tickerC:
				w.Locks[name].Lock()
				if len(batch) > 0 {
					process(batch)
					batch = []ChanEntry{}
				}
				w.Locks[name].Unlock()
			}
		}
	}()
}

// ProcessSites processes a batch of Sites messages
func (w *Worker) ProcessSites(entries []ChanEntry) {
	w.Logger.Debugf("Processing %d sites.", len(entries))

	var siteUUIDs []string
	messagesMap := map[string][]ChanEntry{}
	for _, entry := range entries {
		entryUUID := entry.ReindexMessage.Object.ID
		// dedup
		if !slices.Scontains(siteUUIDs, entryUUID) {
			siteUUIDs = append(siteUUIDs, entryUUID)
			messagesMap[entryUUID] = append(messagesMap[entryUUID], entry)
		} else {
			entry.NSQMessage.Finish()
		}
	}

	w.Logger.Infof("Processing reindex for sites: %s", strings.Join(siteUUIDs, ", "))

	messagesToFinish, err := w.ReindexService.ReindexSites(siteUUIDs, entries[0].ReindexMessage.IndexName)
	if err == nil {
		w.Logger.Infof("Will finish Site messages for: %s", strings.Join(messagesToFinish, ", "))
		for _, uuid := range messagesToFinish {
			messages := messagesMap[uuid]
			for _, msg := range messages {
				msg.NSQMessage.Finish()
			}
		}
	}
}

// ProcessSiteLocations processes a batch of site locations messages
func (w *Worker) ProcessSiteLocations(entries []ChanEntry) {
	w.Logger.Debugf("Processing site locations for %d sites.", len(entries))

	var siteUUIDs []string
	messagesMap := map[string][]ChanEntry{}
	for _, entry := range entries {
		entryUUID := entry.ReindexMessage.Object.ID
		// dedup
		if !slices.Scontains(siteUUIDs, entryUUID) {
			siteUUIDs = append(siteUUIDs, entryUUID)
			messagesMap[entryUUID] = append(messagesMap[entryUUID], entry)
		} else {
			entry.NSQMessage.Finish()
		}
	}

	messagesToFinish, err := w.ReindexService.ReindexSitesLocations(siteUUIDs, entries[0].ReindexMessage.IndexName)
	if err == nil {
		w.Logger.Infof("Will finish SiteLocations messages for: %s", strings.Join(messagesToFinish, ", "))
		for _, uuid := range messagesToFinish {
			messages := messagesMap[uuid]
			for _, msg := range messages {
				msg.NSQMessage.Finish()
			}
		}
	}
}
