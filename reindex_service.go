package site

import (
	"context"
	"encoding/json"
	"fmt"

	"bitbucket.org/canopei/golibs/elasticsearch"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

// ReindexService is the implementation of a reindex service (from DB to ES)
type ReindexService struct {
	Logger               *logrus.Entry
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	SiteService          sitePb.SiteServiceClient
}

// NewReindexService creates a new ReindexService instance
func NewReindexService(
	logger *logrus.Entry,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
	siteService sitePb.SiteServiceClient,
) *ReindexService {
	return &ReindexService{
		Logger:               logger,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		SiteService:          siteService,
	}
}

// ReindexSites indexes the sites from DB to ES
func (s *ReindexService) ReindexSites(siteUUIDs []string, indexName string) ([]string, error) {
	// Get the sites from DB
	sites, err := s.SiteService.GetSites(context.Background(), &sitePb.GetSitesRequest{Uuids: siteUUIDs})
	if err != nil {
		s.Logger.Errorf("Cannot fetch sites by UUIDs: %v", err)
		return nil, fmt.Errorf("Cannot fetch sites by UUIDs: %v", err)
	}
	dbSites := sites.Sites

	s.Logger.Infof("Reindexing %d sites.", len(dbSites))

	for _, dbSite := range dbSites {
		if dbSite.IsApproved {
			indexSiteToES(s.ElasticsearchService, indexName, dbSite)
		}

		// Add to the queue a sync for Locations message
		messageData := queueHelper.NewReindexSiteLocationsMessage(dbSite.Uuid, indexName, queueHelper.ReindexStatusStart)
		message, _ := json.Marshal(messageData)
		s.Queue.Publish("reindex", message)

		// Add to the queue a sync for Locations message
		messageData = queueHelper.NewReindexSiteStaffMessage(dbSite.Uuid, indexName, queueHelper.ReindexStatusStart)
		message, _ = json.Marshal(messageData)
		s.Queue.Publish("reindex", message)

		// Add to the queue a finished message for this site (only for the site component)
		messageData = queueHelper.NewReindexSiteMessage(dbSite.Uuid, indexName, queueHelper.ReindexStatusDone)
		message, _ = json.Marshal(messageData)
		s.Queue.Publish("reindex", message)
	}

	return siteUUIDs, nil
}

// ReindexSitesLocations indexes the locations of multiple sites from DB to ES
func (s *ReindexService) ReindexSitesLocations(siteUUIDs []string, indexName string) ([]string, error) {
	for _, siteUUID := range siteUUIDs {
		result, err := s.SiteService.GetSitesLocations(context.Background(), &sitePb.GetSitesLocationsRequest{Uuids: siteUUID})
		if err != nil {
			s.Logger.Errorf("Cannot fetch the locations for site '%s': %v", siteUUID, err)
			return nil, fmt.Errorf("Cannot fetch the locations for site '%s': %v", siteUUID, err)
		}
		dbLocations := result.Locations

		s.Logger.Infof("Reindexing %d locations for site '%s'.", len(dbLocations), siteUUID)

		for _, dbLocation := range dbLocations {
			if dbLocation.IsApproved {
				indexLocationToES(s.ElasticsearchService, indexName, siteUUID, dbLocation)
			}
		}

		// Add to the queue a sync for Locations message
		messageData := queueHelper.NewReindexSiteClassSchedulesMessage(siteUUID, indexName, queueHelper.ReindexStatusStart)
		message, _ := json.Marshal(messageData)
		s.Queue.Publish("reindex", message)

		// Add to the queue a finished message for this site (only for the locations component)
		messageData = queueHelper.NewReindexSiteLocationsMessage(siteUUID, indexName, queueHelper.ReindexStatusDone)
		message, _ = json.Marshal(messageData)
		s.Queue.Publish("reindex", message)
	}

	return siteUUIDs, nil
}
