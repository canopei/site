package main

import (
	"context"
	"encoding/json"
	"flag"
	"io/ioutil"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	"time"

	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/logging"
	"bitbucket.org/canopei/site"
	"bitbucket.org/canopei/site/config"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	nsq "github.com/nsqio/go-nsq"

	queueHelper "bitbucket.org/canopei/golibs/queue"
)

var (
	conf                 *config.Config
	logger               *logrus.Entry
	queue                *nsq.Producer
	elasticsearchService *elasticsearch.Service
	siteService          sitePb.SiteServiceClient
	version              string

	progress              site.ReindexProgress
	progressCheckStopChan chan bool
	reindexStartTime      time.Time
	isTimeout             bool
)

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("SITE_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "runreindex",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Info("Starting reindex (%s)...", version)

	// connect to the Site service
	siteService, _, err = site.NewClient(conf.SiteService.Addr)
	if err != nil {
		logger.Fatalf("Cannot connect to the site service: %v", err)
	}

	// connect as a producer to the queue service
	logger.Infof("Connecting to the queue service at %s", conf.Queue.Addr)
	cfg := nsq.NewConfig()
	queue, err = nsq.NewProducer(conf.Queue.Addr, cfg)
	if err != nil {
		logger.Fatalf("Cannot start the queue Producer: %v", err)
	}
	queue.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)

	// initialize the ES service
	logger.Infof("Connecting to the ES service at %s", conf.Elasticsearch.Addr)
	elasticsearchService, err = elasticsearch.NewService(&conf.Elasticsearch, logging.CloneLogrusEntry(logger))
	if err != nil {
		logger.Fatalf("Cannot initialize the Elasticsearch service: %v", err)
	}

	// Configure the NSQ consumer
	consumerConfig := nsq.NewConfig()
	consumerConfig.MaxInFlight = conf.Sync.MaxInFlight
	consumerConfig.LookupdPollInterval = time.Duration(conf.Sync.ReconnectInterval) * time.Second

	q, _ := nsq.NewConsumer(conf.Queue.ReindexTopic, "reindex_progress", consumerConfig)
	q.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)
	q.AddHandler(nsq.HandlerFunc(handleQueueMessage))
	err = q.ConnectToNSQD(conf.Queue.Addr)
	if err != nil {
		logger.Fatal("Could not connect to NSQd")
	}

	isTimeout = false
	reindexJob()

	// Gracefully handle SIGINT and SIGTERM
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case <-q.StopChan:
			if isTimeout {
				logger.Error("Reached timeout. Reindex failed. Exiting.")
				os.Exit(1)
			}
			return
		case <-progressCheckStopChan:
			logger.Infof("Reindex finished in %d seconds.", int(time.Since(reindexStartTime).Seconds()))
			q.Stop()
		case <-sigChan:
			q.Stop()
		}
	}
}

func reindexJob() {
	reindexStartTime = time.Now()
	progress = site.ReindexProgress{}

	// Create the ES index
	ctx := context.Background()
	indexNameToDuplicate := "sng_schema"
	// Check that we have the the sng_schema index
	schemaIndexExists, _ := elasticsearchService.Client.IndexExists().Index([]string{indexNameToDuplicate}).Do(ctx)
	if !schemaIndexExists {
		logger.Errorf("Schema index '%s' does not exist.", indexNameToDuplicate)
		return
	}
	getMapping, _ := elasticsearchService.Client.GetMapping().Index(indexNameToDuplicate).Do(ctx)
	getSettings, _ := elasticsearchService.Client.IndexGetSettings().Index(indexNameToDuplicate).Name("index.number_of_shards", "index.number_of_replicas", "index.analysis.*").Do(ctx)

	newIndexName := "sng_" + time.Now().Format("20060102150405")
	newIndexMapping, _ := getMapping[indexNameToDuplicate].(map[string]interface{})
	newIndexMapping["settings"] = getSettings[indexNameToDuplicate].Settings["index"]

	createIndex, err := elasticsearchService.Client.CreateIndex(newIndexName).BodyJson(newIndexMapping).Do(ctx)
	if err != nil || !createIndex.Acknowledged {
		logger.Errorf("Failed to create a new index: %v", err)
		return
	}

	// Get the sites and push to queue
	result, err := siteService.GetSites(context.Background(), &sitePb.GetSitesRequest{})
	if err != nil {
		logger.Errorf("Error while fetching the sites.")
		return
	}
	logger.Infof("[Job] Got %d sites to reindex. Pushing to queue.", len(result.Sites))
	for _, site := range result.Sites {
		progress[site.Uuid] = map[string]bool{}

		messageData := queueHelper.NewReindexSiteMessage(site.Uuid, newIndexName, queueHelper.ReindexStatusStart)
		message, _ := json.Marshal(messageData)
		queue.Publish(conf.Queue.ReindexTopic, message)
	}

	progressTimeoutTimer := time.NewTimer(time.Minute * time.Duration(conf.Sync.ReindexTimeout))
	go func() {
		<-progressTimeoutTimer.C
		isTimeout = true
	}()

	progressCheckStopChan = schedule(func() {
		done := true

		logger.Infof("Reindex progress: %#v", progress)

		componentsToCheck := []queueHelper.ObjectType{
			queueHelper.ObjectTypeSite,
			queueHelper.ObjectTypeSiteLocations,
			queueHelper.ObjectTypeSitesStaff,
			queueHelper.ObjectTypeSiteClassSchedules,
		}

	CheckLoop:
		for _, progressSteps := range progress {
			for _, component := range componentsToCheck {
				_, siteDone := progressSteps[string(component)]
				if !siteDone {
					done = false
					break CheckLoop
				}
			}
		}

		if isTimeout {
			progressCheckStopChan <- true
		}

		if done {
			logger.Info("Reindex finished. Switching alias indexes.")
			// Switch alias
			_, _ = elasticsearchService.Client.Alias().Add(newIndexName, "sng").Do(ctx)

			// Index cleanup
			var sngIndexeRegex = regexp.MustCompile(`^sng_[0-9]{14}$`)
			allIndexesNames, _ := elasticsearchService.Client.IndexNames()
			for _, indexName := range allIndexesNames {
				if sngIndexeRegex.MatchString(indexName) && indexName != newIndexName {
					elasticsearchService.Client.DeleteIndex(indexName).Do(ctx)
				}
			}

			progressCheckStopChan <- true
		}
	}, time.Second*10)
}

func handleQueueMessage(message *nsq.Message) error {
	// Do not auto-finish or requeue messages
	message.DisableAutoResponse()
	message.Finish()

	// If we cannot unmarshal it into a generic queue message, then drop it
	reindexMessage := &queueHelper.ReindexMessage{}
	err := json.Unmarshal(message.Body, reindexMessage)
	if err != nil {
		logger.Warningf("Unable to unmarshal message: %v", message)
		return nil
	}

	if reindexMessage.Status == queueHelper.ReindexStatusDone {
		if _, ok := progress[reindexMessage.Object.ID]; ok {
			progress[reindexMessage.Object.ID][string(reindexMessage.ObjectType)] = true
		}
	}

	return nil
}

func schedule(what func(), delay time.Duration) chan bool {
	stop := make(chan bool)

	go func() {
		for {
			what()
			select {
			case <-time.After(delay):
			case <-stop:
				return
			}
		}
	}()

	return stop
}
