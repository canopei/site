#!/bin/sh
set -e

DELETE=false

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -h|--host)
    HOST="$2"
    shift # past argument
    ;;
    -d)
    DELETE=true
    ;;
    *)
    # unknown option
    ;;
esac
shift # past argument or value
done

if [ -z $HOST ]; then
    echo "Usage: es_indices.sh [-d] -h <es_addr>"
    echo "\t -d\tWill delete the indices if they already exists."
    exit 1
fi

$DELETE && curl -X DELETE "http://${HOST}/sng_schema"

curl -X PUT "http://${HOST}/sng_schema" -d '{
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 1
    },
    "mappings": {
        "site": {
            "_all": { "enabled": false },
            "properties": {}
        },
        "location": {
            "_all": { "enabled": false },
            "_parent": {
                "type": "site"
            },
            "properties": {
                "locationName": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "locationDescription": {
                    "type": "text",
                    "analyzer": "english"
                },
                "cityNA": {
                    "type": "keyword"
                },
                "locationCity": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "locationAddress": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "locationAddress2": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "postalCode": {
                    "type": "keyword"
                },
                "hasClasses": {
                    "type": "boolean"
                },
                "latitude": {
                    "type": "double"
                },
                "longitude": {
                    "type": "double"
                },
                "featured": {
                    "type": "short"
                },
                "trending": {
                    "type": "short"
                },
                "createdAt": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss||epoch_millis"
                }
            }
        },
        "staff": {
            "_all": { "enabled": false },
            "_parent": {
                "type": "site"
            },
            "properties": {
                "staffName": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "staffFirstName": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "staffLastName": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "gender": {
                    "type": "byte"
                },
                "featured": {
                    "type": "short"
                },
                "cityNA": {
                    "type": "keyword"
                },
                "staffNameNA": {
                    "type": "keyword"
                }
            }
        },
        "class": {
            "_all": { "enabled": false },
            "_parent": {
                "type": "location"
            },
            "properties": {
                "className": {
                    "type": "text",
                    "analyzer": "standard"
                },
                "classDescription": {
                    "type": "text",
                    "analyzer": "english"
                },
                "levelId": {
                    "type": "integer"
                },
                "sessionType": {
                    "properties": {
                        "sessionTypeId": {
                            "type": "keyword"
                        },
                        "sessionTypeName": {
                            "type": "text",
                            "analyzer": "english"
                        },
                        "program": {
                            "properties": {
                                "programId": {
                                    "type": "keyword"
                                },
                                "programName": {
                                    "type": "text",
                                    "analyzer": "english"
                                },
                                "scheduleType": {
                                    "type": "byte"
                                }
                            }
                        }
                    }
                },
                "staff": {
                    "properties": {
                        "uuid": {
                            "type": "keyword"
                        },
                        "featured": {
                            "type": "short"
                        }
                    }
                },
                "location": {
                    "properties": {
                        "uuid": {
                            "type": "keyword"
                        }
                    }
                },
                "active": {
                    "type": "boolean"
                },
                "isAvailable": {
                    "type": "boolean"
                },
                "cityNA": {
                    "type": "keyword"
                },
                "startDatetime": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss||epoch_millis"
                },
                "endDatetime": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss||epoch_millis"
                }
            }
        }
    }
}'