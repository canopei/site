package main

import (
	"database/sql"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"golang.org/x/net/context"

	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateCity creates a City
func (s *Server) CreateCity(ctx context.Context, req *sitePb.City) (*sitePb.City, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateCity: %v", req)
	defer timeTrack(logger, time.Now(), "CreateCity")

	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Error("The city name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city name is required.")
	}
	req.Slug = strings.TrimSpace(req.Slug)
	if req.Slug == "" {
		logger.Error("The city slug is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city slug is required.")
	}

	// check unique slug and name
	rows, err := s.DB.Queryx(`SELECT city_id FROM city WHERE name = ? OR slug = ? AND deleted_at = '0'`, req.Name, req.Slug)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing cities.")
	}
	if rows.Next() {
		logger.Errorf("A city already exists with this name or slug.")
		return nil, grpc.Errorf(codes.AlreadyExists, "")
	}

	result, err := s.DB.NamedExec(`INSERT INTO city (name, slug, sort_order, image_url, longitude, latitude)
		VALUES (:name, :slug, :sort_order, :image_url, :longitude, :latitude)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created city '%s'.", req.Name)

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Failed retrieving the last insert ID.")
	}

	var city = &sitePb.City{}
	err = s.DB.Get(city, "SELECT * FROM city WHERE city_id = ?", lastInsertID)
	if err != nil {
		logger.Errorf("Error while fetching the new city: %v", err)
		return nil, err
	}

	return city, nil
}

// GetCity retrieves a City by ID
func (s *Server) GetCity(ctx context.Context, req *sitePb.City) (*sitePb.City, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetCity: %v", req)
	defer timeTrack(logger, time.Now(), "GetCity")

	// Validations
	if req.Id < 1 {
		logger.Errorf("The city ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city ID is required.")
	}

	var city sitePb.City
	err := s.DB.Get(&city, `SELECT * FROM city WHERE city_id = ? AND deleted_at = '0'`, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find city ID %d.", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching city")
	}

	// Get subcities
	city.Subcities = []string{}
	rows, err := s.DB.Queryx(`SELECT name FROM city_group WHERE city_id = ?`, city.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the subcities")
	}

	for rows.Next() {
		var subcityName string
		err := rows.Scan(&subcityName)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the subcity")
		}
		city.Subcities = append(city.Subcities, subcityName)
	}

	return &city, nil
}

// GetCities retrieves every city
func (s *Server) GetCities(ctx context.Context, req *empty.Empty) (*sitePb.CitiesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetCities: %v", req)
	defer timeTrack(logger, time.Now(), "GetCities")

	// Batch fetch the subcities
	subcitiesMap := map[int32][]string{}
	rows, err := s.DB.Queryx(`SELECT city_id, name FROM city_group`)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the subcities")
	}
	for rows.Next() {
		var subcityName string
		var cityID int32

		err := rows.Scan(&cityID, &subcityName)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the subcity")
		}

		if _, ok := subcitiesMap[cityID]; ok {
			subcitiesMap[cityID] = append(subcitiesMap[cityID], subcityName)
		} else {
			subcitiesMap[cityID] = []string{subcityName}
		}
	}

	rows, err = s.DB.Queryx(`SELECT * FROM city WHERE deleted_at = '0' ORDER BY sort_order DESC`)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the cities")
	}
	citiesList := &sitePb.CitiesList{}
	for rows.Next() {
		city := sitePb.City{}
		err := rows.StructScan(&city)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the city")
		}
		if subcities, ok := subcitiesMap[city.Id]; ok {
			city.Subcities = subcities
		}

		citiesList.Cities = append(citiesList.Cities, &city)
	}

	return citiesList, nil
}

// UpdateCity updates a City
func (s *Server) UpdateCity(ctx context.Context, req *sitePb.City) (*sitePb.City, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateCity: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateCity")

	// Validations
	if req.Id < 1 {
		logger.Error("The city ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city ID is required.")
	}
	req.Slug = strings.TrimSpace(req.Slug)
	if req.Slug == "" {
		logger.Error("The city slug is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city slug is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Error("The city name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city name is required.")
	}

	// check unique slug and name
	rows, err := s.DB.Queryx(`SELECT city_id FROM city WHERE name = ? OR slug = ? AND deleted_at = '0' AND city_id <> ?`, req.Name, req.Slug, req.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing cities.")
	}
	if rows.Next() {
		logger.Errorf("A city already exists with this name or slug.")
		return nil, grpc.Errorf(codes.AlreadyExists, "")
	}

	_, err = s.DB.NamedExec(`UPDATE city SET
			name = :name, slug = :slug, sort_order = :sort_order, image_url = :image_url,
			longitude = :longitude, latitude = :latitude
		WHERE deleted_at = '0' AND id = :city_id`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var city sitePb.City
	err = s.DB.Get(&city, "SELECT * FROM city WHERE id = ?", req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateCity - city not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return &city, nil
}

// DeleteCity removes a City (sets deleted_at)
func (s *Server) DeleteCity(ctx context.Context, req *sitePb.City) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteCity: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteCity")

	if req.Id < 1 {
		logger.Error("The city ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city ID is required.")
	}

	result, err := s.DB.Exec("UPDATE city SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND city_id = ?", req.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteCity - city not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// AddSubcity adds a city by name to the given city group
func (s *Server) AddSubcity(ctx context.Context, req *sitePb.SubcityRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("AddSubcity: %v", req)
	defer timeTrack(logger, time.Now(), "AddSubcity")

	// Validations
	if req.CityId < 1 {
		logger.Error("The city ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city ID is required.")
	}
	req.SubcityName = strings.TrimSpace(req.SubcityName)
	if req.SubcityName == "" {
		logger.Error("The subcity name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The subscity name is required.")
	}

	// Check that city ID exists
	rows, err := s.DB.Query("SELECT city_id FROM city where city_id = ?", req.CityId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "SQL error while checking existing city ID.")
	}
	if !rows.Next() {
		logger.Error("Invalid city ID provided.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Invalid city ID provided.")
	}

	// Check for duplicate
	rows, err = s.DB.Query("SELECT city_group_id FROM city_group WHERE name = ?", req.SubcityName)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "SQL error while checking for duplicate.")
	}
	if rows.Next() {
		logger.Error("The city already belongs to a group.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city already belongs to a group.")
	}

	_, err = s.DB.Exec(`INSERT INTO city_group (city_id, name) VALUES (?, ?)`, req.CityId, req.SubcityName)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Added city '%s' to city group ID %d.", req.SubcityName, req.CityId)

	return &empty.Empty{}, nil
}

// DeleteSubcity removes a city by name from the given city group
func (s *Server) DeleteSubcity(ctx context.Context, req *sitePb.SubcityRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteSubcity: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteSubcity")

	// Validations
	if req.CityId < 1 {
		logger.Error("The city ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The city ID is required.")
	}
	req.SubcityName = strings.TrimSpace(req.SubcityName)
	if req.SubcityName == "" {
		logger.Error("The subcity name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The subscity name is required.")
	}

	result, err := s.DB.Exec("DELETE FROM city_group WHERE city_id = ? AND name = ?", req.CityId, req.SubcityName)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot delete database entry.")
	}
	affected, _ := result.RowsAffected()
	if affected == 0 {
		logger.Errorf("DeleteSubcity - city not found: %s, group %d", req.SubcityName, req.CityId)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
