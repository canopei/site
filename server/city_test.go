package main

import (
	"context"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"

	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var (
	cityColumns = []string{"city_id", "name", "slug", "image_url", "sort_order", "longitude", "latitude", "created_at", "modified_at", "deleted_at"}
)

func TestCreateCityValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.City
		ExpectedCode codes.Code
	}{
		{&sitePb.City{}, codes.InvalidArgument},
		{&sitePb.City{Slug: "foo-bar"}, codes.InvalidArgument},
		{&sitePb.City{Name: "foo"}, codes.InvalidArgument},
		{&sitePb.City{Name: " ", Slug: "foo-bar"}, codes.InvalidArgument},
		{&sitePb.City{Name: "foo", Slug: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.City{Name: "foo", Slug: "foo-bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		city, err := server.CreateCity(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(city, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateCitySuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(sqlmock.NewRows([]string{"city_id"}))
	mock.ExpectExec("^INSERT INTO city").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(cityColumns).
		AddRow(1, "name", "slug", "/image1.jpg", "10", "12.1234", "-12.1234", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(rows)

	city, err := server.CreateCity(context.Background(), &sitePb.City{
		Name:      "name",
		Slug:      "slug",
		Longitude: 12.1234,
		Latitude:  -23.1234,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(city)
	if city != nil {
		assert.Equal("name", city.Name)
	}
}

func TestCreateCityExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(sqlmock.NewRows([]string{"city_id"}).AddRow(1))

	city, err := server.CreateCity(context.Background(), &sitePb.City{
		Slug: "slug",
		Name: "name",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(city)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetCityValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.City
		ExpectedCode codes.Code
	}{
		{&sitePb.City{}, codes.InvalidArgument},
		{&sitePb.City{Id: 0}, codes.InvalidArgument},
		{&sitePb.City{Id: -1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.City{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		result, err := server.GetCity(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(result, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetCitySuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testCity := &sitePb.City{
		Id:   1,
		Name: "name",
		Slug: "slug",
	}

	citiesRows := sqlmock.NewRows(cityColumns).
		AddRow(testCity.Id, testCity.Name, testCity.Slug, "/image1.jpg", "10", "12.1234", "-12.1234", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(citiesRows)

	subcitiesRows := sqlmock.NewRows([]string{"name"}).AddRow("subcity1").AddRow("subcity2")
	mock.ExpectQuery("^SELECT (.+) FROM city_group").WillReturnRows(subcitiesRows)

	city, err := server.GetCity(context.Background(), &sitePb.City{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(city)
	if city != nil {
		assert.Equal("name", city.Name)
	}
}

func TestGetCityMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(sqlmock.NewRows(cityColumns))

	response, err := server.GetCity(context.Background(), &sitePb.City{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(response)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetCitiesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	subcitiesRows := sqlmock.NewRows([]string{"city_id", "name"}).
		AddRow(1, "subcity1").
		AddRow(1, "subcity2").
		AddRow(2, "subcity3").
		AddRow(2, "subcity4").
		AddRow(3, "subcity5")
	mock.ExpectQuery("^SELECT (.+) FROM city_group").WillReturnRows(subcitiesRows)

	cities := []*sitePb.City{
		&sitePb.City{Id: 1, Name: "foo", Slug: "foo-"},
		&sitePb.City{Id: 2, Name: "bar", Slug: "bar-"},
		&sitePb.City{Id: 3, Name: "foobar", Slug: "foobar-"},
	}
	citiesRows := sqlmock.NewRows(cityColumns)
	for _, city := range cities {
		citiesRows.AddRow(city.Id, city.Name, city.Slug, city.ImageUrl, "10", "12.1234", "-12.1234", "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(citiesRows)

	response, err := server.GetCities(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response.Cities != nil {
		assert.Len(response.Cities, len(cities))
		assert.Equal("bar", response.Cities[1].Name)
	}
}

func TestGetCitiesMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	subcitiesRows := sqlmock.NewRows([]string{"city_id", "name"}).
		AddRow(1, "subcity1").
		AddRow(1, "subcity2").
		AddRow(2, "subcity3").
		AddRow(2, "subcity4").
		AddRow(3, "subcity5")
	mock.ExpectQuery("^SELECT (.+) FROM city_group").WillReturnRows(subcitiesRows)

	mock.ExpectQuery("^SELECT (.+) FROM city WHERE").WillReturnRows(sqlmock.NewRows(cityColumns))

	response, err := server.GetCities(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Cities, 0)
	}
}

func TestUpdateCityValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.City
		ExpectedCode codes.Code
	}{
		{&sitePb.City{}, codes.InvalidArgument},
		{&sitePb.City{Id: 1, Slug: "foo-bar"}, codes.InvalidArgument},
		{&sitePb.City{Id: 1, Name: "foo"}, codes.InvalidArgument},
		{&sitePb.City{Id: 1, Name: " ", Slug: "foo-bar"}, codes.InvalidArgument},
		{&sitePb.City{Name: "foo", Slug: "foo-bar"}, codes.InvalidArgument},
		{&sitePb.City{Id: 1, Name: "foo", Slug: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.City{Id: 1, Name: "foo", Slug: "foo-bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		city, err := server.UpdateCity(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(city, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateCityMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// the validation
	mock.ExpectQuery("^SELECT (.+) FROM city WHERE").WillReturnRows(sqlmock.NewRows(cityColumns))
	mock.ExpectExec("^UPDATE city SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM city WHERE").WillReturnRows(sqlmock.NewRows(cityColumns))

	city, err := server.UpdateCity(context.Background(), &sitePb.City{Id: 1, Name: "foo", Slug: "foo-bar"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(city)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateCitySuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testCity := &sitePb.City{
		Id:        1,
		Name:      "name",
		Slug:      "slug",
		Longitude: 12.1234,
		Latitude:  -23.1234,
	}

	// the validation
	mock.ExpectQuery("^SELECT (.+) FROM city WHERE").WillReturnRows(sqlmock.NewRows(cityColumns))
	mock.ExpectExec("^UPDATE city SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(cityColumns).
		AddRow(testCity.Id, testCity.Name, testCity.Slug, "/image1.jpg", "10", "12.1234", "-12.1234", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM city WHERE").WithArgs(testCity.Id).WillReturnRows(rows)

	city, err := server.UpdateCity(context.Background(), testCity)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(city)
}

func TestUpdateCityAlreadyExistingNameOrSlug(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testCity := &sitePb.City{
		Id:   1,
		Name: "name",
		Slug: "slug",
	}

	// the validation
	mock.ExpectQuery("^SELECT (.+) FROM city WHERE").WillReturnRows(sqlmock.NewRows([]string{"city_id"}).AddRow(1))

	city, err := server.UpdateCity(context.Background(), testCity)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(city)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestDeleteCityValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.City
		ExpectedCode codes.Code
	}{
		{&sitePb.City{}, codes.InvalidArgument},
		{&sitePb.City{Id: 0}, codes.InvalidArgument},
		{&sitePb.City{Id: -1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.City{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		result, err := server.DeleteCity(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(result, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteCity(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE city SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))
	_, err = server.DeleteCity(context.Background(), &sitePb.City{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE city SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteCity(context.Background(), &sitePb.City{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestAddSubcityValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.SubcityRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.SubcityRequest{}, codes.InvalidArgument},
		{&sitePb.SubcityRequest{CityId: 1, SubcityName: " "}, codes.InvalidArgument},
		{&sitePb.SubcityRequest{CityId: 1}, codes.InvalidArgument},
		{&sitePb.SubcityRequest{SubcityName: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.SubcityRequest{CityId: 1, SubcityName: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		result, err := server.AddSubcity(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(result, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestAddSubcitySuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Checks
	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(sqlmock.NewRows([]string{"city_id"}).AddRow(1))
	mock.ExpectQuery("^SELECT (.+) FROM city_group").WillReturnRows(sqlmock.NewRows([]string{"city_group_id"}))

	mock.ExpectExec("^INSERT INTO city_group").WillReturnResult(sqlmock.NewResult(1, 1))

	response, err := server.AddSubcity(context.Background(), &sitePb.SubcityRequest{CityId: 1, SubcityName: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
}

func TestAddSubcityInvalidCityID(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Checks
	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(sqlmock.NewRows([]string{"city_id"}))

	response, err := server.AddSubcity(context.Background(), &sitePb.SubcityRequest{CityId: 1, SubcityName: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(response)
	assert.Equal(codes.InvalidArgument, grpc.Code(err))
}

func TestAddSubcityDuplicate(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Checks
	mock.ExpectQuery("^SELECT (.+) FROM city").WillReturnRows(sqlmock.NewRows([]string{"city_id"}).AddRow(1))
	mock.ExpectQuery("^SELECT (.+) FROM city_group").WillReturnRows(sqlmock.NewRows([]string{"city_group_id"}).AddRow(1))

	response, err := server.AddSubcity(context.Background(), &sitePb.SubcityRequest{CityId: 1, SubcityName: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(response)
	assert.Equal(codes.InvalidArgument, grpc.Code(err))
}

func TestDeleteSubcityValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.SubcityRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.SubcityRequest{}, codes.InvalidArgument},
		{&sitePb.SubcityRequest{CityId: 1, SubcityName: " "}, codes.InvalidArgument},
		{&sitePb.SubcityRequest{CityId: 1}, codes.InvalidArgument},
		{&sitePb.SubcityRequest{SubcityName: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.SubcityRequest{CityId: 1, SubcityName: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		result, err := server.DeleteSubcity(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(result, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteSubcitySuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^DELETE FROM city_group").WillReturnResult(sqlmock.NewResult(0, 1))

	response, err := server.DeleteSubcity(context.Background(), &sitePb.SubcityRequest{CityId: 1, SubcityName: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
}

func TestDeleteSubcityMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^DELETE FROM city_group").WillReturnResult(sqlmock.NewResult(0, 0))

	response, err := server.DeleteSubcity(context.Background(), &sitePb.SubcityRequest{CityId: 1, SubcityName: "foo"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(response)
	assert.Equal(codes.NotFound, grpc.Code(err))
}
