package main

import (
	"database/sql"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"

	"golang.org/x/net/context"

	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	cStrings "bitbucket.org/canopei/golibs/strings"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateLocation creates a Location
func (s *Server) CreateLocation(ctx context.Context, req *sitePb.Location) (*sitePb.Location, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateLocation: %v", req)
	defer timeTrack(logger, time.Now(), "CreateLocation")

	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Error("The location name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location name is required.")
	}

	// Check if already exists first if we have a MB ID
	if req.MbId != 0 {
		rows, err := s.DB.Queryx(`SELECT location_id FROM location WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing location.")
		}
		if rows.Next() {
			logger.Errorf("A location already exists with this MB ID %d for site ID %d.", req.MbId, req.SiteId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	// Create the location
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	rand.Seed(time.Now().Unix())
	req.Slug = fmt.Sprintf("%s-%d", cStrings.Slugify(req.Name), rand.Intn(999)+1000)

	_, err = s.DB.NamedExec(`INSERT INTO location
		(uuid, site_id, mb_id, name, description, slug, phone, city, address, address2, postal_code, has_classes, latitude, longitude)
		VALUES (unhex(replace(:uuid,'-','')), :site_id, :mb_id, :name, :description, :slug, :phone, :city, :address, :address2,
		:postal_code, :has_classes, :latitude, :longitude)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created location '%s'.", req.GetUuid())

	var location = &sitePb.Location{}
	err = s.DB.Get(location, `SELECT
			l.*,
			s.uuid "site_uuid",
			COALESCE(c.name, l.city) "city_group_name"
		FROM location l
		INNER JOIN site s ON s.site_id = l.site_id
		LEFT JOIN city_group cg ON cg.name = l.city
		LEFT JOIN city c ON c.city_id = cg.city_id
		WHERE l.uuid = UNHEX(REPLACE(?,'-',''))`, req.Uuid)
	if err != nil {
		logger.Errorf("Error while fetching the new location: %v", err)
		return nil, err
	}
	location.Uuid = uuid.String()

	unpackedUUID, err := crypto.Parse([]byte(location.SiteUuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the site UUID")
	}
	location.SiteUuid = unpackedUUID.String()

	return location, nil
}

// GetLocations retrieves multiple Locations by UUIDs or IDs
func (s *Server) GetLocations(ctx context.Context, req *sitePb.GetLocationsRequest) (*sitePb.LocationsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetLocations: %v", req)
	defer timeTrack(logger, time.Now(), "GetLocations")

	req.Uuids = strings.TrimSpace(req.Uuids)
	reqUuids := []string{}
	if req.Uuids != "" {
		uuids := strings.Split(req.Uuids, ",")
		for _, uuid := range uuids {
			uuid := strings.TrimSpace(uuid)
			if uuid != "" {
				reqUuids = append(reqUuids, uuid)
			}
		}
	}

	// Validations
	if len(reqUuids) == 0 && len(req.Ids) == 0 {
		logger.Errorf("Either locations UUIDs or the IDs is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Either the locations UUIDs or the IDs is required.")
	}
	if len(reqUuids) > 0 && len(req.Ids) > 0 {
		logger.Errorf("Only one of locations UUIDs or the IDs should be provided.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Only one of locations UUIDs or the IDs should be provided.")
	}

	// Fetch the locations
	queryParams := map[string]interface{}{}

	condition := ""
	if len(reqUuids) > 0 {
		condition = "l.uuid IN ("
		for k, v := range reqUuids {
			paramKey := "uuid" + strconv.Itoa(k)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	if len(req.Ids) > 0 {
		condition = "l.location_id IN ("
		for k, v := range req.Ids {
			paramKey := "id" + strconv.Itoa(k)
			condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	rows, err := s.DB.NamedQuery(`SELECT
			l.*,
			s.uuid "site_uuid",
			COALESCE(c.name, l.city) "city_group_name"
		FROM location l
		INNER JOIN site s ON s.site_id = l.site_id
		LEFT JOIN city_group cg ON cg.name = l.city
		LEFT JOIN city c ON c.city_id = cg.city_id
		WHERE `+condition+`
			AND l.deleted_at = '0'`, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the locations")
	}

	locationsList := &sitePb.LocationsList{}
	for rows.Next() {
		location := sitePb.Location{}
		err := rows.StructScan(&location)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the locations")
		}

		unpackedUUID, err := crypto.Parse([]byte(location.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		location.Uuid = unpackedUUID.String()

		unpackedUUID, err = crypto.Parse([]byte(location.SiteUuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the site UUID")
		}
		location.SiteUuid = unpackedUUID.String()

		locationsList.Locations = append(locationsList.Locations, &location)
	}

	locationIDs := []int32{}
	for _, location := range locationsList.Locations {
		locationIDs = append(locationIDs, location.Id)
	}

	// Get photos
	locationsPhotos, err := s.getPhotosByLocationIDs(locationIDs)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the photos")
	}
	for _, location := range locationsList.Locations {
		location.Photos = locationsPhotos[location.Id]
	}

	// Get opening hours
	locationsOpeningHours, err := s.getOpeningHoursByLocationIDs(locationIDs)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the opening hours")
	}
	for _, location := range locationsList.Locations {
		location.OpeningHours = locationsOpeningHours[location.Id]
	}

	return locationsList, nil
}

// GetLocationByMBID retrieves a Location by Mindbody ID
func (s *Server) GetLocationByMBID(ctx context.Context, req *sitePb.GetLocationRequest) (*sitePb.Location, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetLocationByMBID: %v", req)
	defer timeTrack(logger, time.Now(), "GetLocationByMBID")

	// Validations
	if req.Id == 0 {
		logger.Errorf("The location MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location MB ID is required.")
	}
	req.SiteUuid = strings.TrimSpace(req.SiteUuid)
	if req.SiteUuid == "" {
		logger.Errorf("The site UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site UUID is required.")
	}

	var location sitePb.Location
	err := s.DB.Get(&location, `SELECT
			l.*,
			s.uuid "site_uuid",
			COALESCE(c.name, l.city) "city_group_name"
		FROM location l
		INNER JOIN site s ON s.site_id = l.site_id
		LEFT JOIN city_group cg ON cg.name = l.city
		LEFT JOIN city c ON c.city_id = cg.city_id
		WHERE l.mb_id = ?
			AND s.uuid = UNHEX(REPLACE(?,'-',''))
			AND l.deleted_at = '0'
			AND s.deleted_at = '0'`, req.Id, req.SiteUuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find location MB ID %d.", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching site")
	}

	unpackedUUID, err := crypto.Parse([]byte(location.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	location.Uuid = unpackedUUID.String()

	unpackedUUID, err = crypto.Parse([]byte(location.SiteUuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the site UUID")
	}
	location.SiteUuid = unpackedUUID.String()

	// Get the photos
	locationsPhotos, err := s.getPhotosByLocationIDs([]int32{location.Id})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the photos")
	}
	location.Photos = locationsPhotos[location.Id]

	openingHours, err := s.getOpeningHoursByLocationIDs([]int32{location.Id})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the opening hours.")
	}
	location.OpeningHours = openingHours[location.Id]

	return &location, nil
}

// GetLocationBySlug retrieves a Location by the given slug
func (s *Server) GetLocationBySlug(ctx context.Context, req *sitePb.GetBySlugRequest) (*sitePb.Location, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetLocationBySlug: %v", req)
	defer timeTrack(logger, time.Now(), "GetLocationBySlug")

	// Validations
	req.Slug = strings.TrimSpace(req.Slug)
	if req.Slug == "" {
		logger.Errorf("The slug is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The slug is required.")
	}

	var location sitePb.Location
	err := s.DB.Get(&location, `SELECT
			l.*,
			s.uuid "site_uuid",
			COALESCE(c.name, l.city) "city_group_name"
		FROM location l
		INNER JOIN site s ON s.site_id = l.site_id
		LEFT JOIN city_group cg ON cg.name = l.city
		LEFT JOIN city c ON c.city_id = cg.city_id
		WHERE l.slug = ? AND l.deleted_at = '0'`, req.Slug)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find location with the slug '%s'.", req.Slug)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching location")
	}

	unpackedUUID, err := crypto.Parse([]byte(location.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	location.Uuid = unpackedUUID.String()

	unpackedUUID, err = crypto.Parse([]byte(location.SiteUuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the site UUID")
	}
	location.SiteUuid = unpackedUUID.String()

	// Get the photos
	locationsPhotos, err := s.getPhotosByLocationIDs([]int32{location.Id})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the photos")
	}
	location.Photos = locationsPhotos[location.Id]

	openingHours, err := s.getOpeningHoursByLocationIDs([]int32{location.Id})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the opening hours.")
	}
	location.OpeningHours = openingHours[location.Id]

	return &location, nil
}

// UpdateLocation updates a Location
func (s *Server) UpdateLocation(ctx context.Context, req *sitePb.Location) (*sitePb.Location, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateLocation: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateLocation")

	// Validations
	if req.Uuid == "" {
		logger.Error("The location UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location UUID is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Error("The location name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location name is required.")
	}
	// Validate opening hours if provided
	if req.OpeningHours != nil {
		for _, oh := range req.OpeningHours {
			oh.StartTime = strings.TrimSpace(oh.StartTime)
			oh.EndTime = strings.TrimSpace(oh.EndTime)
			if oh.StartTime == "" || oh.EndTime == "" {
				logger.Error("A valid opening hours interval is required.")
				return nil, grpc.Errorf(codes.InvalidArgument, "A valid opening hours interval is required.")
			}
			if oh.Weekday < 0 || oh.Weekday > 6 {
				logger.Error("A valid opening hours weekday is required.")
				return nil, grpc.Errorf(codes.InvalidArgument, "A valid opening hours weekday is required.")
			}
		}
	}

	var location sitePb.Location
	err := s.DB.Get(&location, "SELECT * FROM location WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateLocation - location not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - first select.")
	}
	// TODO: if we change the approved status, validate the ACL level
	if location.IsApproved != req.IsApproved {
		// check ACL
	}

	rand.Seed(time.Now().Unix())
	req.Slug = fmt.Sprintf("%s-%d", cStrings.Slugify(req.Name), rand.Intn(999)+1000)

	_, err = s.DB.NamedExec(`UPDATE location SET
			description = :description, phone = :phone, city = :city, address = :address,
			address2 = :address2, postal_code = :postal_code, has_classes = :has_classes, latitude = :latitude,
			longitude = :longitude, is_approved = :is_approved, late_cancel_interval = :late_cancel_interval,
			featured = CASE WHEN :featured = -1 THEN featured ELSE :featured END,
			trending = CASE WHEN :trending = -1 THEN trending ELSE :trending END,
			slug = CASE WHEN name != :name THEN :slug ELSE slug END,
			name = :name
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	err = s.DB.Get(&location, `SELECT
			l.*,
			s.uuid "site_uuid",
			COALESCE(c.name, l.city) "city_group_name"
		FROM location l
		INNER JOIN site s ON s.site_id = l.site_id
		LEFT JOIN city_group cg ON cg.name = l.city
		LEFT JOIN city c ON c.city_id = cg.city_id
		WHERE l.uuid = UNHEX(REPLACE(?,'-',''))`, req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateLocation - location not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	location.Uuid = req.Uuid

	unpackedUUID, err := crypto.Parse([]byte(location.SiteUuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the site UUID")
	}
	location.SiteUuid = unpackedUUID.String()

	// If we should update the opening hours
	if req.OpeningHours != nil {
		_, err = s.DB.Exec(`DELETE FROM location_opening_hours WHERE location_id = ?`, location.Id)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Unable to query database - delete existing opening hours entries.")
		}

		for _, oh := range req.OpeningHours {
			oh.LocationId = location.Id

			_, err = s.DB.NamedExec(`INSERT INTO location_opening_hours (location_id, weekday, start_time, end_time)
				VALUES (:location_id, :weekday, :start_time, :end_time)`, oh)
			if err != nil {
				return nil, grpcUtils.InternalError(logger, err, "Unable to query database - insert opening hours entries.")
			}
		}
	}

	// Get the photos
	locationsPhotos, err := s.getPhotosByLocationIDs([]int32{location.Id})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the photos")
	}
	location.Photos = locationsPhotos[location.Id]

	openingHours, err := s.getOpeningHoursByLocationIDs([]int32{location.Id})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the opening hours.")
	}
	location.OpeningHours = openingHours[location.Id]

	return &location, nil
}

// DeleteLocation removes a Location (sets deleted_at)
func (s *Server) DeleteLocation(ctx context.Context, req *sitePb.DeleteLocationRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteLocation: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteLocation")

	if req.Uuid == "" {
		logger.Error("The location UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location UUID is required.")
	}

	result, err := s.DB.Exec("UPDATE location SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteLocation - location not found: %s", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetSitesLocations retrieves the Locations of a given Site
func (s *Server) GetSitesLocations(ctx context.Context, req *sitePb.GetSitesLocationsRequest) (*sitePb.LocationsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSitesLocations: %v", req)
	defer timeTrack(logger, time.Now(), "GetSitesLocations")

	uuids := map[string]bool{}
	rawUuids := strings.Split(req.Uuids, ",")
	for _, v := range rawUuids {
		v = strings.TrimSpace(v)
		if v != "" {
			uuids[v] = true
		}
	}
	if len(uuids) == 0 {
		logger.Error("At least one site UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one site UUID is required.")
	}

	// Fetch the locations
	queryParams := map[string]interface{}{}
	condition := "AND s.uuid IN ("
	idx := 0
	for k := range uuids {
		paramKey := "id" + strconv.Itoa(idx)
		condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
		queryParams[paramKey] = k
		idx++
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	rows, err := s.DB.NamedQuery(`SELECT
			l.*,
			s.uuid "site_uuid",
			COALESCE(c.name, l.city) "city_group_name"
		FROM location l
		INNER JOIN site s ON s.site_id = l.site_id
		LEFT JOIN city_group cg ON cg.name = l.city
		LEFT JOIN city c ON c.city_id = cg.city_id
		WHERE s.deleted_at = '0' AND l.deleted_at = '0' `+condition, queryParams)

	if err != nil {
		logger.Errorf("Error while fetching the locations: %v", err)
		return nil, err
	}

	locationsList := &sitePb.LocationsList{}
	for rows.Next() {
		location := sitePb.Location{}
		err := rows.StructScan(&location)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the locations")
		}

		unpackedUUID, err := crypto.Parse([]byte(location.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		location.Uuid = unpackedUUID.String()

		unpackedUUID, err = crypto.Parse([]byte(location.SiteUuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the site UUID")
		}
		location.SiteUuid = unpackedUUID.String()

		locationsList.Locations = append(locationsList.Locations, &location)
	}

	locationIDs := []int32{}
	for _, location := range locationsList.Locations {
		locationIDs = append(locationIDs, location.Id)
	}

	// Get photos
	locationsPhotos, err := s.getPhotosByLocationIDs(locationIDs)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the photos")
	}
	for _, location := range locationsList.Locations {
		location.Photos = locationsPhotos[location.Id]
	}

	// Get opening hours
	locationsOpeningHours, err := s.getOpeningHoursByLocationIDs(locationIDs)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the opening hours")
	}
	for _, location := range locationsList.Locations {
		location.OpeningHours = locationsOpeningHours[location.Id]
	}

	return locationsList, nil
}

// GetSitesLocationsByMBID retrieves the Locations for a given list of Site MBIDs
func (s *Server) GetSitesLocationsByMBID(ctx context.Context, req *sitePb.GetSitesLocationsByMBIDRequest) (*sitePb.LocationsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSitesLocationsByMBID: %v", req)
	defer timeTrack(logger, time.Now(), "GetSitesLocationsByMBID")

	mbIDs := map[int32]bool{}
	rawMBIDs := strings.Split(req.MbIds, ",")
	for _, v := range rawMBIDs {
		v = strings.TrimSpace(v)
		intV, err := strconv.ParseInt(v, 10, 32)
		if err == nil {
			mbIDs[int32(intV)] = true
		}
	}
	if len(mbIDs) == 0 {
		logger.Error("At least one site MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one site MB ID is required.")
	}

	// Fetch the locations
	queryParams := map[string]interface{}{}
	condition := "AND s.mb_id IN ("
	idx := 0
	for k := range mbIDs {
		paramKey := "id" + strconv.Itoa(idx)
		condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
		queryParams[paramKey] = k
		idx++
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	rows, err := s.DB.NamedQuery(`SELECT
			l.*,
			s.uuid "site_uuid",
			COALESCE(c.name, l.city) "city_group_name"
		FROM location l
		INNER JOIN site s ON s.site_id = l.site_id
		LEFT JOIN city_group cg ON cg.name = l.city
		LEFT JOIN city c ON c.city_id = cg.city_id
		WHERE s.deleted_at = '0' AND l.deleted_at = '0'`+condition, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the locations: %v", err)
		return nil, err
	}

	locationsList := &sitePb.LocationsList{}
	for rows.Next() {
		location := sitePb.Location{}
		err := rows.StructScan(&location)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the locations")
		}

		unpackedUUID, err := crypto.Parse([]byte(location.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		location.Uuid = unpackedUUID.String()

		unpackedUUID, err = crypto.Parse([]byte(location.SiteUuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the site UUID")
		}
		location.SiteUuid = unpackedUUID.String()

		locationsList.Locations = append(locationsList.Locations, &location)
	}

	locationIDs := []int32{}
	for _, location := range locationsList.Locations {
		locationIDs = append(locationIDs, location.Id)
	}

	// Get photos
	locationsPhotos, err := s.getPhotosByLocationIDs(locationIDs)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the photos")
	}
	for _, location := range locationsList.Locations {
		location.Photos = locationsPhotos[location.Id]
	}

	// Get opening hours
	locationsOpeningHours, err := s.getOpeningHoursByLocationIDs(locationIDs)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the opening hours")
	}
	for _, location := range locationsList.Locations {
		location.OpeningHours = locationsOpeningHours[location.Id]
	}

	return locationsList, nil
}

// SyncLocationsFromDBToES syncs the given location by UUIDs from DB to ES
func (s *Server) SyncLocationsFromDBToES(ctx context.Context, req *sitePb.SyncLocationsFromDBToESRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("SyncLocationsFromDBToES: %v", req)
	defer timeTrack(logger, time.Now(), "SyncLocationsFromDBToES")

	req.Uuids = strings.TrimSpace(req.Uuids)
	if len(req.Uuids) > 0 {
		_, err := s.SyncService.SyncLocationsFromDB(strings.Split(req.Uuids, ","))
		if err != nil {
			logger.Errorf("Error while syncing the locations: %v", err)
			return nil, grpcUtils.InternalError(logger, err, "Error while syncing the locations")
		}
	}

	return &empty.Empty{}, nil
}

// CreateLocationFavourite creates a new LocationFavourite
func (s *Server) CreateLocationFavourite(ctx context.Context, req *sitePb.LocationFavouriteRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateLocationFavourite: %v", req)
	defer timeTrack(logger, time.Now(), "CreateLocationFavourite")

	// Validations
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	if req.LocationId < 1 {
		logger.Errorf("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}

	_, err := s.DB.Exec(`INSERT IGNORE INTO location_favourite (location_id, account_id) VALUES (?, ?)`, req.LocationId, req.AccountId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	logger.Debugf("Created new location favourite for location %d and account %d.", req.LocationId, req.AccountId)

	return &empty.Empty{}, nil
}

// DeleteLocationFavourite removes a LocationFavourite
func (s *Server) DeleteLocationFavourite(ctx context.Context, req *sitePb.LocationFavouriteRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteLocationFavourite: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteLocationFavourite")

	// Validations
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	if req.LocationId < 1 {
		logger.Errorf("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}

	_, err := s.DB.Exec(`DELETE FROM location_favourite WHERE location_id = ? AND account_id = ?`, req.LocationId, req.AccountId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot delete database entry.")
	}

	logger.Debugf("Removed location favourite for location %d and account %d.", req.LocationId, req.AccountId)

	return &empty.Empty{}, nil
}

// GetLocationFavouriteForAccount fethces the LocationFavourite for the given account ID
func (s *Server) GetLocationFavouriteForAccount(ctx context.Context, req *sitePb.GetLocationFavouriteForAccountRequest) (*sitePb.GetLocationFavouriteForAccountResponse, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetLocationFavouriteForAccount: %v", req)
	defer timeTrack(logger, time.Now(), "GetLocationFavouriteForAccount")

	// Validations
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT l.uuid
		FROM location_favourite lf
		INNER JOIN location l ON lf.location_id = l.location_id
		WHERE l.deleted_at = '0' AND lf.account_id = ?
		ORDER BY lf.created_at DESC`, req.AccountId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the location favourites")
	}

	locationUuids := []string{}
	for rows.Next() {
		var uuid string

		err := rows.Scan(&uuid)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the location favourites")
		}

		unpackedUUID, err := crypto.Parse([]byte(uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for location favourite.")
		}
		uuid = unpackedUUID.String()

		locationUuids = append(locationUuids, uuid)
	}

	return &sitePb.GetLocationFavouriteForAccountResponse{LocationUuids: locationUuids}, nil
}

func (s *Server) getPhotosByLocationIDs(locationIDs []int32) (map[int32][]*sitePb.LocationPhoto, error) {
	locationsPhotos := map[int32][]*sitePb.LocationPhoto{}

	if len(locationIDs) == 0 {
		return locationsPhotos, nil
	}

	for _, locationID := range locationIDs {
		locationsPhotos[locationID] = []*sitePb.LocationPhoto{}
	}

	query, args, _ := sqlx.In("SELECT * FROM location_photo WHERE location_id IN (?) AND deleted_at = '0'", locationIDs)
	query = s.DB.Rebind(query)
	rows, err := s.DB.Queryx(query, args...)
	if err != nil {
		return nil, fmt.Errorf("Error while fetching the location phones")
	}
	for rows.Next() {
		photo := &sitePb.LocationPhoto{}
		err := rows.StructScan(photo)
		if err != nil {
			return nil, fmt.Errorf("Error while scanning the location photos")
		}

		unpackedUUID, err := crypto.Parse([]byte(photo.Uuid))
		if err != nil {
			return nil, fmt.Errorf("Cannot unpack the UUID")
		}
		photo.Uuid = unpackedUUID.String()

		locationsPhotos[photo.LocationId] = append(locationsPhotos[photo.LocationId], photo)
	}

	return locationsPhotos, nil
}

func (s *Server) getOpeningHoursByLocationIDs(locationIDs []int32) (map[int32][]*sitePb.LocationOpeningHours, error) {
	locationsOpeningHours := map[int32][]*sitePb.LocationOpeningHours{}

	if len(locationIDs) == 0 {
		return locationsOpeningHours, nil
	}

	for _, locationID := range locationIDs {
		locationsOpeningHours[locationID] = []*sitePb.LocationOpeningHours{}
	}

	query, args, _ := sqlx.In("SELECT * FROM location_opening_hours WHERE location_id IN (?) ORDER BY weekday ASC, start_time ASC", locationIDs)
	query = s.DB.Rebind(query)
	rows, err := s.DB.Queryx(query, args...)
	if err != nil {
		return nil, fmt.Errorf("Error while fetching the location opening hours")
	}
	for rows.Next() {
		oh := &sitePb.LocationOpeningHours{}
		err := rows.StructScan(oh)
		if err != nil {
			return nil, fmt.Errorf("Error while scanning the location opening hours")
		}

		locationsOpeningHours[oh.LocationId] = append(locationsOpeningHours[oh.LocationId], oh)
	}

	return locationsOpeningHours, nil
}
