package main

import (
	"database/sql"
	"strings"
	"time"

	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/golang/protobuf/ptypes/empty"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	sitePb "bitbucket.org/canopei/site/protobuf"
)

// CreateLocationPhoto creates a new LocationPhoto
func (s *Server) CreateLocationPhoto(ctx context.Context, req *sitePb.LocationPhoto) (*sitePb.LocationPhoto, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateLocationPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "CreateLocationPhoto")

	// Validations
	req.Path = strings.TrimSpace(req.Path)
	if req.Path == "" {
		logger.Errorf("The photo file path is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo file path is required.")
	}
	req.LocationUuid = strings.TrimSpace(req.LocationUuid)
	if req.LocationUuid == "" {
		logger.Errorf("The location UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location UUID is required.")
	}

	// Check the location
	var location sitePb.Location
	err := s.DB.Get(&location, `SELECT * FROM location WHERE uuid = UNHEX(REPLACE(?,'-','')) AND deleted_at = '0'`, req.LocationUuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find location '%s'.", req.LocationUuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching location")
	}
	req.LocationId = location.Id

	// Create a UUID
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	result, err := s.DB.NamedExec(`INSERT INTO location_photo (uuid, location_id, path, caption)
		VALUES (unhex(replace(:uuid,'-','')), :location_id, :path, :caption)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot fetch the last inserted ID.")
	}

	var photo = &sitePb.LocationPhoto{}
	err = s.DB.Get(photo, "SELECT * FROM location_photo WHERE location_photo_id = ?", lastInsertID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, grpcUtils.InternalError(logger, err, "Cannot find the new location photo.")
		}

		logger.Errorf("Error while fetching the new location photo: %v", err)
		return nil, err
	}
	photo.Uuid = uuid.String()

	logger.Debugf("Created new location photo %d.", photo.Id)

	return photo, nil
}

// GetLocationPhoto retrieves a LocationPhoto by UUID
func (s *Server) GetLocationPhoto(ctx context.Context, req *sitePb.LocationPhoto) (*sitePb.LocationPhoto, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetLocationPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "GetLocationPhoto")

	// Validations
	req.LocationUuid = strings.TrimSpace(req.LocationUuid)
	if req.LocationUuid == "" {
		logger.Errorf("The location UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location UUID is required.")
	}
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Errorf("The photo UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo UUID is required.")
	}

	var photo sitePb.LocationPhoto
	err := s.DB.Get(&photo, `SELECT lp.*
				FROM location_photo lp
				INNER JOIN location l ON l.location_id = lp.location_id
				WHERE l.uuid = UNHEX(REPLACE(?,'-',''))
					AND lp.uuid = UNHEX(REPLACE(?,'-',''))
					AND l.deleted_at = '0'
					AND lp.deleted_at = '0'`, req.LocationUuid, req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find location photo UUID '%s' for location '%s'.", req.Uuid, req.LocationUuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the location photo")
	}
	photo.Uuid = req.Uuid

	return &photo, nil
}

// GetLocationPhotos retrieves the LocationPhotos of a Location
func (s *Server) GetLocationPhotos(ctx context.Context, req *sitePb.LocationPhoto) (*sitePb.LocationPhotoList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetLocationPhotos: %v", req)
	defer timeTrack(logger, time.Now(), "GetLocationPhotos")

	// Validations
	req.LocationUuid = strings.TrimSpace(req.LocationUuid)
	if req.LocationUuid == "" {
		logger.Errorf("The location UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location UUID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT lp.*
		FROM location_photo lp
		INNER JOIN location l ON l.location_id = lp.location_id
		WHERE l.uuid = UNHEX(REPLACE(?,'-','')) AND l.deleted_at = '0' AND lp.deleted_at = '0'`, req.LocationUuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the location photos")
	}

	locationPhotosList := &sitePb.LocationPhotoList{}
	for rows.Next() {
		photo := &sitePb.LocationPhoto{}
		err := rows.StructScan(photo)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a location photo")
		}

		unpackedUUID, err := crypto.Parse([]byte(photo.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		photo.Uuid = unpackedUUID.String()

		locationPhotosList.Photos = append(locationPhotosList.Photos, photo)
	}

	return locationPhotosList, nil
}

// UpdateLocationPhoto updates a LocationPhoto
func (s *Server) UpdateLocationPhoto(ctx context.Context, req *sitePb.LocationPhoto) (*sitePb.LocationPhoto, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateLocationPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateLocationPhoto")

	// Validations
	req.LocationUuid = strings.TrimSpace(req.LocationUuid)
	if req.LocationUuid == "" {
		logger.Errorf("The location UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location UUID is required.")
	}
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Errorf("The photo UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo UUID is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE location_photo lp
		INNER JOIN location l ON l.location_id = lp.location_id
		SET lp.path = :path, lp.caption = :caption
		WHERE l.uuid = UNHEX(REPLACE(:location_uuid,'-',''))
			AND lp.uuid = UNHEX(REPLACE(:uuid,'-',''))
			AND l.deleted_at = '0'
			AND lp.deleted_at = '0'`, req)

	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var photo sitePb.LocationPhoto
	err = s.DB.Get(&photo, `SELECT lp.*
		FROM location_photo lp
		INNER JOIN location l ON l.location_id = lp.location_id
		WHERE l.uuid = UNHEX(REPLACE(?,'-',''))
			AND lp.uuid = UNHEX(REPLACE(?,'-',''))
			AND l.deleted_at = '0'
			AND lp.deleted_at = '0'`, req.LocationUuid, req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateLocationPhoto - location photo not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	photo.Uuid = req.Uuid

	return &photo, nil
}

// DeleteLocationPhoto removes a LocationPhoto (sets deleted_at)
func (s *Server) DeleteLocationPhoto(ctx context.Context, req *sitePb.LocationPhoto) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteLocationPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteLocationPhoto")

	// Validations
	req.LocationUuid = strings.TrimSpace(req.LocationUuid)
	if req.LocationUuid == "" {
		logger.Errorf("The location UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location UUID is required.")
	}
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Errorf("The photo UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo UUID is required.")
	}

	result, err := s.DB.Exec(`UPDATE location_photo lp
		INNER JOIN location l ON l.location_id = lp.location_id
		SET lp.deleted_at = CURRENT_TIMESTAMP
		WHERE l.uuid = UNHEX(REPLACE(?,'-',''))
			AND lp.uuid = UNHEX(REPLACE(?,'-',''))
			AND l.deleted_at = '0'
			AND lp.deleted_at = '0'`, req.LocationUuid, req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteProgram - location photo not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
