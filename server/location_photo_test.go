package main

import (
	"context"
	"testing"

	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	locationPhotoColumns = []string{"location_photo_id", "uuid", "location_id", "path", "caption", "created_at", "modified_at", "deleted_at"}
)

func TestCreateLocationPhotoValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.LocationPhoto
		ExpectedCode codes.Code
	}{
		{&sitePb.LocationPhoto{}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{Path: " "}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{Path: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.LocationPhoto{Path: "foo", LocationUuid: "1234567890123456"}, codes.Unknown},
		{&sitePb.LocationPhoto{Path: "foo", LocationUuid: "1234567890123456", Caption: " "}, codes.Unknown},
		{&sitePb.LocationPhoto{Path: "foo", LocationUuid: "1234567890123456", Caption: "test caption"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.CreateLocationPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateLocationPhotoSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows(locationColumns).
		AddRow(1, "123456789012345", 123, 1, "name", "description", "phone", "city", "address", "address2", "postal_code",
			true, 1, 1, 11.1234, -12.2345, 1, "2017-01-01", "2017-01-02", "2017-01-03", "1234567890123456", "city group name"))
	mock.ExpectExec("^INSERT INTO location_photo").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(locationPhotoColumns).AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(rows)

	photo, err := server.CreateLocationPhoto(context.Background(), &sitePb.LocationPhoto{
		Path:         "foo",
		LocationUuid: "1234567890123456",
		Caption:      "bar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(photo)
	if photo != nil {
		assert.Equal("bar", photo.Caption)
	}
}

func TestGetLocationPhotosValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.LocationPhoto
		ExpectedCode codes.Code
	}{
		{&sitePb.LocationPhoto{}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{LocationUuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.LocationPhoto{LocationUuid: "1234567890123456"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.GetLocationPhotos(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetLocationPhotosSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(locationPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(rows)

	response, err := server.GetLocationPhotos(context.Background(), &sitePb.LocationPhoto{LocationUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Photos, 2)
	}
}

func TestGetLocationPhotosEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(locationPhotoColumns)
	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(rows)

	response, err := server.GetLocationPhotos(context.Background(), &sitePb.LocationPhoto{LocationUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Photos, 0)
	}
}

func TestGetLocationPhotoValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.LocationPhoto
		ExpectedCode codes.Code
	}{
		{&sitePb.LocationPhoto{}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{Uuid: "1234567890123456"}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{LocationUuid: "1234567890123456"}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.LocationPhoto{Uuid: "1234567890123456", LocationUuid: "1234567890123456"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.GetLocationPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetLocationPhotoSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(locationPhotoColumns).AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(rows)

	photo, err := server.GetLocationPhoto(context.Background(), &sitePb.LocationPhoto{Uuid: "1234567890123456", LocationUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(photo)
	if photo != nil {
		assert.Equal("bar", photo.Caption)
	}
}

func TestGetLocationPhotoMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns))

	photo, err := server.GetLocationPhoto(context.Background(), &sitePb.LocationPhoto{Uuid: "1234567890123456", LocationUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(photo)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateProgramValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.LocationPhoto
		ExpectedCode codes.Code
	}{
		{&sitePb.LocationPhoto{}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{LocationUuid: "1234567890123456"}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{Uuid: "1234567890123457"}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{LocationUuid: " ", Uuid: "1234567890123457"}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{LocationUuid: "1234567890123456", Uuid: " "}, codes.InvalidArgument},

		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.LocationPhoto{Path: "foo", LocationUuid: "1234567890123456", Uuid: "1234567890123457"}, codes.Unknown},
		{&sitePb.LocationPhoto{Path: "foo", LocationUuid: "1234567890123456", Uuid: "1234567890123457", Caption: " "}, codes.Unknown},
		{&sitePb.LocationPhoto{Path: "foo", LocationUuid: "1234567890123456", Uuid: "1234567890123457", Caption: "test caption"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.UpdateLocationPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateLocationPhotoMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE location_photo").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns))

	photo, err := server.UpdateLocationPhoto(context.Background(), &sitePb.LocationPhoto{Path: "foo", LocationUuid: "1234567890123456", Uuid: "1234567890123457", Caption: "test caption"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(photo)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateLocationPhotoSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testPhoto := &sitePb.LocationPhoto{
		LocationUuid: "1234567890123457",
		Uuid:         "1234567890123456",
		Path:         "/test/path.jpeg",
		Caption:      "foo",
	}

	mock.ExpectExec("^UPDATE location_photo(.+)SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(locationPhotoColumns).AddRow(1, testPhoto.Uuid, 1, testPhoto.Path, testPhoto.Caption, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(rows)

	photo, err := server.UpdateLocationPhoto(context.Background(), testPhoto)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(photo)
	// sanity check
	if photo != nil {
		assert.Equal(testPhoto.Caption, photo.Caption)
	}
}

func TestDeleteLocationPhotoValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.LocationPhoto
		ExpectedCode codes.Code
	}{
		{&sitePb.LocationPhoto{}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{Uuid: "1234567890123456"}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{LocationUuid: "1234567890123456"}, codes.InvalidArgument},
		{&sitePb.LocationPhoto{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.LocationPhoto{Uuid: "1234567890123456", LocationUuid: "1234567890123456"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		result, err := server.DeleteLocationPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(result, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteLocationPhoto(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE location_photo (.+)SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteLocationPhoto(context.Background(), &sitePb.LocationPhoto{Uuid: "1234567890123456", LocationUuid: "1234567890123456"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE location_photo (.+)SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteLocationPhoto(context.Background(), &sitePb.LocationPhoto{Uuid: "1234567890123456", LocationUuid: "1234567890123456"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
