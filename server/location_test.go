package main

import (
	"context"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"

	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var (
	locationColumns = []string{"location_id", "uuid", "mb_id", "site_id", "name", "description", "phone",
		"city", "address", "address2", "postal_code", "has_classes", "featured", "trending", "latitude",
		"longitude", "is_approved", "created_at", "modified_at", "deleted_at", "site_uuid", "city_group_name"}

	openingHoursColumns = []string{"location_opening_hours_id", "location_id", "weekday", "start_time", "end_time"}
)

func TestCreateLocationValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.Location
		ExpectedCode codes.Code
	}{
		{&sitePb.Location{SiteId: 1}, codes.InvalidArgument},
		{&sitePb.Location{Name: "bar"}, codes.InvalidArgument},
		{&sitePb.Location{SiteId: 1, Name: " "}, codes.InvalidArgument},
		{&sitePb.Location{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.Location{SiteId: 1, Name: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.CreateLocation(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateLocationSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows([]string{"location_id"}))
	mock.ExpectExec("^INSERT INTO location").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(locationColumns).
		AddRow(1, "123456789012345", 123, 1, "name", "description", "phone", "city", "address", "address2", "postal_code",
			true, 1, 1, 11.1234, -12.2345, 1, "2017-01-01", "2017-01-02", "2017-01-03", "1234567890123456", "city group name")
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(rows)

	location, err := server.CreateLocation(context.Background(), &sitePb.Location{
		SiteId:      1,
		Name:        "name",
		MbId:        123,
		Description: "description",
		Latitude:    12.2345,
		Longitude:   -12.2345,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(location)
	if location != nil {
		assert.NotEmpty(location.Uuid)
		assert.Equal("name", location.Name)
	}
}

func TestCreateLocationExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows([]string{"location_id"}).AddRow(1))

	location, err := server.CreateLocation(context.Background(), &sitePb.Location{
		SiteId:      1,
		Name:        "name",
		MbId:        123,
		Description: "description",
		Latitude:    12.2345,
		Longitude:   -12.2345,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(location)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetLocationsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetLocationsRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetLocationsRequest{}, codes.InvalidArgument},
		{&sitePb.GetLocationsRequest{Ids: []int32{1}, Uuids: "uuid"}, codes.InvalidArgument},
		{&sitePb.GetLocationsRequest{Uuids: " ,"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetLocationsRequest{Ids: []int32{1}}, codes.Unknown},
		{&sitePb.GetLocationsRequest{Uuids: "uuid"}, codes.Unknown},
		{&sitePb.GetLocationsRequest{Uuids: "uuid1, uuuid2"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.GetLocations(context.Background(), validationTest.Request)

		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}
func TestGetLocationsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	locations := []*sitePb.Location{
		&sitePb.Location{Id: 1, Uuid: "1234567890123456", MbId: 123, SiteId: 2, Name: "foo"},
		&sitePb.Location{Id: 2, Uuid: "1234567890123456", MbId: 124, SiteId: 2, Name: "bar"},
		&sitePb.Location{Id: 3, Uuid: "1234567890123456", MbId: 125, SiteId: 1, Name: "foobar"},
	}
	locationsRows := sqlmock.NewRows(locationColumns)
	for _, location := range locations {
		locationsRows.AddRow(location.Id, location.Uuid, location.MbId, location.SiteId, location.Name, "description", "phone",
			"city", "address", "address2", "postal_code", 1, 1, 1, 11.1234, -12.2345, 1, "2017-01-01", "2017-01-02", "2017-01-03",
			"1234567890123456", "city group name")
	}
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(locationsRows)

	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 2, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(3, "1234567890123457", 2, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM location_opening_hours").WillReturnRows(sqlmock.NewRows(openingHoursColumns).
		AddRow(1, 1, 1, "08:00", "20:00").
		AddRow(2, 2, 0, "08:00", "19:00").
		AddRow(3, 2, 1, "10:00", "12:00").
		AddRow(4, 2, 1, "14:00", "18:00"))

	response, err := server.GetLocations(context.Background(), &sitePb.GetLocationsRequest{Ids: []int32{1}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response.Locations != nil {
		assert.Len(response.Locations, len(locations))
		assert.Equal("bar", response.Locations[1].Name)

		assert.Len(response.Locations[0].Photos, 1)
		assert.Len(response.Locations[1].Photos, 2)
		assert.Len(response.Locations[2].Photos, 0)

		assert.Len(response.Locations[1].OpeningHours, 3)
		assert.Len(response.Locations[2].OpeningHours, 0)
	}
}

func TestGetLocationsMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows(locationColumns))

	response, err := server.GetLocations(context.Background(), &sitePb.GetLocationsRequest{Uuids: "uuid1,uuid2"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Locations, 0)
	}
}

func TestGetLocationByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetLocationRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetLocationRequest{}, codes.InvalidArgument},
		{&sitePb.GetLocationRequest{Id: 0}, codes.InvalidArgument},
		{&sitePb.GetLocationRequest{Id: 1}, codes.InvalidArgument},
		{&sitePb.GetLocationRequest{Id: 1, SiteUuid: ""}, codes.InvalidArgument},
		{&sitePb.GetLocationRequest{Id: 1, SiteUuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetLocationRequest{Id: 1, SiteUuid: "uuid1"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.GetLocationByMBID(context.Background(), validationTest.Request)

		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}
func TestGetLocationByMBIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	locationsRows := sqlmock.NewRows(locationColumns)
	locationsRows.AddRow(1, "1234567890123456", 123, 2, "foo", "description", "phone",
		"city", "address", "address2", "postal_code", 1, 1, 1, 11.1234, -12.2345, 1, "2017-01-01", "2017-01-02", "2017-01-03",
		"1234567890123456", "city group name")
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(locationsRows)

	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM location_opening_hours").WillReturnRows(sqlmock.NewRows(openingHoursColumns).
		AddRow(1, 1, 0, "08:00", "20:00").
		AddRow(2, 1, 1, "08:00", "19:00").
		AddRow(3, 1, 1, "10:00", "12:00"))

	location, err := server.GetLocationByMBID(context.Background(), &sitePb.GetLocationRequest{Id: 1, SiteUuid: "uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(location)
	if location != nil {
		assert.Equal("foo", location.Name)
		assert.Len(location.Photos, 2)
		assert.Len(location.OpeningHours, 3)
	}
}

func TestGetLocationByMBIDMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows(locationColumns))

	location, err := server.GetLocationByMBID(context.Background(), &sitePb.GetLocationRequest{Id: 1, SiteUuid: "uuid"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(location)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetLocationBySlugValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetBySlugRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetBySlugRequest{}, codes.InvalidArgument},
		{&sitePb.GetBySlugRequest{Slug: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetBySlugRequest{Slug: "foo-bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.GetLocationBySlug(context.Background(), validationTest.Request)

		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}
func TestGetLocationBySlugSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	locationsRows := sqlmock.NewRows(locationColumns)
	locationsRows.AddRow(1, "1234567890123456", 123, 2, "foo", "description", "phone",
		"city", "address", "address2", "postal_code", 1, 1, 1, 11.1234, -12.2345, 1, "2017-01-01", "2017-01-02", "2017-01-03",
		"1234567890123456", "city group name")
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(locationsRows)

	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM location_opening_hours").WillReturnRows(sqlmock.NewRows(openingHoursColumns).
		AddRow(1, 1, 0, "08:00", "20:00").
		AddRow(2, 1, 1, "08:00", "19:00").
		AddRow(3, 1, 1, "10:00", "12:00"))

	location, err := server.GetLocationBySlug(context.Background(), &sitePb.GetBySlugRequest{Slug: "foo-bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(location)
	if location != nil {
		assert.Equal("foo", location.Name)
		assert.Len(location.Photos, 2)
		assert.Len(location.OpeningHours, 3)
	}
}

func TestGetLocationBySlugMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows(locationColumns))

	location, err := server.GetLocationBySlug(context.Background(), &sitePb.GetBySlugRequest{Slug: "foo-bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(location)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestDeleteLocationValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.DeleteLocationRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.DeleteLocationRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.DeleteLocationRequest{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.DeleteLocation(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteLocation(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE location SET(.+)deleted_at").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteLocation(context.Background(), &sitePb.DeleteLocationRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE location SET(.+)deleted_at").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteLocation(context.Background(), &sitePb.DeleteLocationRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestUpdateLocationValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.Location
		ExpectedCode codes.Code
	}{
		{&sitePb.Location{}, codes.InvalidArgument},
		{&sitePb.Location{Uuid: "foo"}, codes.InvalidArgument},
		{&sitePb.Location{Name: "bar"}, codes.InvalidArgument},
		{&sitePb.Location{Uuid: "foo", Name: " "}, codes.InvalidArgument},
		{&sitePb.Location{Uuid: "foo", Name: "bar", OpeningHours: []*sitePb.LocationOpeningHours{
			&sitePb.LocationOpeningHours{Weekday: 0, StartTime: "08:00", EndTime: "20:00"},
			&sitePb.LocationOpeningHours{Weekday: 1, StartTime: " ", EndTime: "20:00"},
		}}, codes.InvalidArgument},
		{&sitePb.Location{Uuid: "foo", Name: "bar", OpeningHours: []*sitePb.LocationOpeningHours{
			&sitePb.LocationOpeningHours{Weekday: 0, StartTime: "08:00", EndTime: "20:00"},
			&sitePb.LocationOpeningHours{Weekday: 9, StartTime: "10:00", EndTime: "20:00"},
		}}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.Location{Uuid: "foo", Name: "bar"}, codes.Unknown},
		{&sitePb.Location{Uuid: "foo", Name: "bar", OpeningHours: []*sitePb.LocationOpeningHours{
			&sitePb.LocationOpeningHours{Weekday: 0, StartTime: "08:00", EndTime: "20:00"},
			&sitePb.LocationOpeningHours{Weekday: 1, StartTime: "10:00", EndTime: "20:00"},
		}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.UpdateLocation(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateLocationMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows(locationColumns))

	location, err := server.UpdateLocation(context.Background(), &sitePb.Location{Uuid: "foo", Name: "bar"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(location)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateLocationSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testLocation := &sitePb.Location{
		Uuid:     "123456789012345",
		MbId:     123,
		SiteId:   1,
		Name:     "name",
		Latitude: 12.1234,
		OpeningHours: []*sitePb.LocationOpeningHours{
			&sitePb.LocationOpeningHours{Weekday: 0, StartTime: "08:00", EndTime: "20:00"},
			&sitePb.LocationOpeningHours{Weekday: 1, StartTime: "10:00", EndTime: "20:00"},
		},
	}

	// The check that it exists
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(sqlmock.NewRows(locationColumns).
		AddRow(1, testLocation.Uuid, 123, 1, "name", "description", "phone", "city", "address", "address2", "postal_code",
			true, 1, 1, 12.1234, -12.1234, 1, "2017-01-01", "2017-01-02", "2017-01-03", "1234567890123456", "city group name"))

	mock.ExpectExec("^UPDATE location SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(locationColumns).
		AddRow(1, testLocation.Uuid, 123, 1, "name", "description", "phone", "city", "address", "address2", "postal_code",
			true, 1, 1, 12.1234, -12.1234, 1, "2017-01-01", "2017-01-02", "2017-01-03", "1234567890123456", "city group name")
	mock.ExpectQuery("^SELECT (.+) FROM location").WithArgs(testLocation.Uuid).WillReturnRows(rows)

	mock.ExpectExec("^DELETE FROM location_opening_hours").WillReturnResult(sqlmock.NewResult(0, 2))
	mock.ExpectExec("^INSERT INTO location_opening_hours").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO location_opening_hours").WillReturnResult(sqlmock.NewResult(2, 1))

	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM location_opening_hours").WillReturnRows(sqlmock.NewRows(openingHoursColumns).
		AddRow(1, 1, 0, "08:00", "20:00").
		AddRow(2, 1, 1, "08:00", "19:00").
		AddRow(3, 1, 1, "10:00", "12:00"))

	location, err := server.UpdateLocation(context.Background(), testLocation)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(location)
	assert.Len(location.OpeningHours, 3)
	assert.Len(location.Photos, 2)
}

func TestGetSitesLocationsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetSitesLocationsRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetSitesLocationsRequest{}, codes.InvalidArgument},
		{&sitePb.GetSitesLocationsRequest{Uuids: ""}, codes.InvalidArgument},
		{&sitePb.GetSitesLocationsRequest{Uuids: " ,"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetSitesLocationsRequest{Uuids: ",uuid1,uuid2,"}, codes.Unknown},
		{&sitePb.GetSitesLocationsRequest{Uuids: "uuid1,uuid2"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		locations, err := server.GetSitesLocations(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(locations, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSitesLocationsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	locations := []*sitePb.Location{
		&sitePb.Location{Id: 1, Uuid: "1234567890123456", MbId: 123, SiteId: 2, Name: "foo"},
		&sitePb.Location{Id: 2, Uuid: "1234567890123456", MbId: 124, SiteId: 2, Name: "bar"},
		&sitePb.Location{Id: 3, Uuid: "1234567890123456", MbId: 125, SiteId: 1, Name: "foobar"},
	}
	locationsRows := sqlmock.NewRows(locationColumns)
	for _, location := range locations {
		locationsRows.AddRow(location.Id, location.Uuid, location.MbId, location.SiteId, location.Name, "description", "phone",
			"city", "address", "address2", "postal_code", 1, 1, 1, 11.1234, -12.2345, 1, "2017-01-01", "2017-01-02", "2017-01-03",
			"1234567890123456", "city group name")
	}
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(locationsRows)

	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM location_opening_hours").WillReturnRows(sqlmock.NewRows(openingHoursColumns).
		AddRow(1, 1, 0, "08:00", "20:00").
		AddRow(2, 2, 1, "08:00", "19:00").
		AddRow(3, 2, 1, "10:00", "12:00"))

	response, err := server.GetSitesLocations(context.Background(), &sitePb.GetSitesLocationsRequest{Uuids: "uuid1, uuid2"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Locations, len(locations))

		assert.Len(response.Locations[0].Photos, 2)
		assert.Len(response.Locations[1].Photos, 0)

		assert.Len(response.Locations[2].OpeningHours, 0)
		assert.Len(response.Locations[1].OpeningHours, 2)
	}
}

func TestGetSitesLocationsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	locationsRows := sqlmock.NewRows(locationColumns)
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(locationsRows)

	response, err := server.GetSitesLocations(context.Background(), &sitePb.GetSitesLocationsRequest{Uuids: "uuid1, uuid2"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Locations, 0)
	}
}

func TestGetSitesLocationsByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetSitesLocationsByMBIDRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetSitesLocationsByMBIDRequest{}, codes.InvalidArgument},
		{&sitePb.GetSitesLocationsByMBIDRequest{MbIds: ""}, codes.InvalidArgument},
		{&sitePb.GetSitesLocationsByMBIDRequest{MbIds: " ,"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetSitesLocationsByMBIDRequest{MbIds: ",1234,1234567,"}, codes.Unknown},
		{&sitePb.GetSitesLocationsByMBIDRequest{MbIds: "1234,1234567"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		locations, err := server.GetSitesLocationsByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(locations, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSitesLocationsByMBIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	locations := []*sitePb.Location{
		&sitePb.Location{Id: 1, Uuid: "1234567890123456", MbId: 123, SiteId: 2, Name: "foo"},
		&sitePb.Location{Id: 2, Uuid: "1234567890123456", MbId: 124, SiteId: 2, Name: "bar"},
		&sitePb.Location{Id: 3, Uuid: "1234567890123456", MbId: 125, SiteId: 1, Name: "foobar"},
	}
	locationsRows := sqlmock.NewRows(locationColumns)
	for _, location := range locations {
		locationsRows.AddRow(location.Id, location.Uuid, location.MbId, location.SiteId, location.Name, "description", "phone",
			"city", "address", "address2", "postal_code", 1, 1, 1, 11.1234, -12.2345, 1, "2017-01-01", "2017-01-02", "2017-01-03",
			"1234567890123456", "city group name")
	}
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(locationsRows)

	mock.ExpectQuery("^SELECT (.+) FROM location_photo").WillReturnRows(sqlmock.NewRows(locationPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM location_opening_hours").WillReturnRows(sqlmock.NewRows(openingHoursColumns).
		AddRow(1, 1, 0, "08:00", "20:00").
		AddRow(2, 2, 1, "08:00", "19:00").
		AddRow(3, 2, 1, "10:00", "12:00"))

	response, err := server.GetSitesLocationsByMBID(context.Background(), &sitePb.GetSitesLocationsByMBIDRequest{MbIds: ",123,456,"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Locations, len(locations))

		assert.Len(response.Locations[0].Photos, 2)
		assert.Len(response.Locations[1].Photos, 0)

		assert.Len(response.Locations[1].OpeningHours, 2)
		assert.Len(response.Locations[2].OpeningHours, 0)
	}
}

func TestGetSitesLocationsByMBIDEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	locationsRows := sqlmock.NewRows(locationColumns)
	mock.ExpectQuery("^SELECT (.+) FROM location").WillReturnRows(locationsRows)

	response, err := server.GetSitesLocationsByMBID(context.Background(), &sitePb.GetSitesLocationsByMBIDRequest{MbIds: "123,456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Locations, 0)
	}
}

func TestCreateLocationFavouriteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.LocationFavouriteRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.LocationFavouriteRequest{}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{AccountId: 1}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{LocationId: 2}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{AccountId: 0, LocationId: 2}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{AccountId: 1, LocationId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.LocationFavouriteRequest{AccountId: 1, LocationId: 2}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.CreateLocationFavourite(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateLocationFavouriteSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT IGNORE INTO location_favourite ").WillReturnResult(sqlmock.NewResult(1, 1))

	response, err := server.CreateLocationFavourite(context.Background(), &sitePb.LocationFavouriteRequest{AccountId: 1, LocationId: 2})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
}

func TestDeleteLocationFavouriteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.LocationFavouriteRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.LocationFavouriteRequest{}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{AccountId: 1}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{LocationId: 2}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{AccountId: 0, LocationId: 2}, codes.InvalidArgument},
		{&sitePb.LocationFavouriteRequest{AccountId: 1, LocationId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.LocationFavouriteRequest{AccountId: 1, LocationId: 2}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteLocationFavourite(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteLocationFavouriteSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^DELETE FROM location_favourite ").WillReturnResult(sqlmock.NewResult(0, 1))

	response, err := server.DeleteLocationFavourite(context.Background(), &sitePb.LocationFavouriteRequest{AccountId: 1, LocationId: 2})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
}

func TestGetLocationFavouriteForAccountValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetLocationFavouriteForAccountRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetLocationFavouriteForAccountRequest{}, codes.InvalidArgument},
		{&sitePb.GetLocationFavouriteForAccountRequest{AccountId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetLocationFavouriteForAccountRequest{AccountId: 123}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.GetLocationFavouriteForAccount(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetLocationFavouriteForAccountSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows([]string{"location_uuid"}).AddRow("1234567890123456").AddRow("1234567890123457")
	mock.ExpectQuery("^SELECT (.+) FROM location_favourite ").WillReturnRows(rows)

	response, err := server.GetLocationFavouriteForAccount(context.Background(), &sitePb.GetLocationFavouriteForAccountRequest{AccountId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.Len(response.LocationUuids, 2)
}
