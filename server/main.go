package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"google.golang.org/grpc"

	"net"

	"bitbucket.org/canopei/golibs/auth"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/healthcheck"
	"bitbucket.org/canopei/golibs/logging"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	siteMb "bitbucket.org/canopei/mindbody/services/site"
	"bitbucket.org/canopei/site"
	"bitbucket.org/canopei/site/config"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	nsq "github.com/nsqio/go-nsq"
)

var (
	conf         *config.Config
	logger       *logrus.Entry
	db           *sqlx.DB
	siteMBClient *siteMb.Site_x0020_ServiceSoap
	saleMBClient *saleMb.Sale_x0020_ServiceSoap
	queue        *nsq.Producer
	version      string
)

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("SITE_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "grpc",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' server (%s)...", conf.Service.Name, version)

	// open a database connection
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s",
		conf.Db.Username,
		conf.Db.Password,
		conf.Db.Addr,
		conf.Db.DbName,
	)
	logger.Infof("Connecting to SQL - DSN: %s", dsn)
	db, err = sqlx.Connect("mysql", fmt.Sprintf(
		"%s:%s@tcp(%s)/%s",
		conf.Db.Username,
		conf.Db.Password,
		conf.Db.Addr,
		conf.Db.DbName,
	))
	if err != nil {
		logger.Fatalf("Unable to connect to the database: %v", err)
	}
	defer db.Close()

	// prepare the MB site SOAP client
	siteMBClient = siteMb.NewSite_x0020_ServiceSoap("", false, nil)

	// prepare the MB sale SOAP client
	saleMBClient = saleMb.NewSale_x0020_ServiceSoap("", false, nil)

	// connect to the queue server
	logger.Infof("Connecting to the queue service at %s", conf.Queue.Addr)
	queue, err = nsq.NewProducer(conf.Queue.Addr, nsq.NewConfig())
	queue.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)

	// start the gRPC server
	s := NewServer(&conf.Mindbody, logger, db, siteMBClient, queue)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Failed to listen on tcp:%d: %v", conf.Service.GrpcPort, err)
	}

	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(auth.NewInterceptor(logger, conf.Auth.JWTSigningKey).UnaryServerInterceptor()),
	}

	grpcServer := grpc.NewServer(opts...)
	sitePb.RegisterSiteServiceServer(grpcServer, s)

	// Setup a health check listener
	go func() {
		logger.Infof("Booting '%s' health check on %d...", conf.Service.Name, conf.Service.HealthPort)
		http.HandleFunc(healthcheck.Healthpath, healthcheck.Handler)
		http.ListenAndServe(fmt.Sprintf(":%d", conf.Service.HealthPort), nil)
	}()

	// Setup the s.SyncService
	selfClient, _, err := site.NewClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot connect to the self service: %v", err)
	}
	esClient, err := elasticsearch.NewService(&conf.Elasticsearch, logging.CloneLogrusEntry(logger))
	if err != nil {
		logger.Fatalf("Cannot setup the ES client: %v", err)
	}
	s.SyncService = site.NewSyncService(logger, siteMBClient, saleMBClient, queue, esClient, selfClient, &conf.Mindbody)

	logger.Infof("Serving on %d.", conf.Service.GrpcPort)

	grpcServer.Serve(lis)
}
