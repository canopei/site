package main

import (
	"golang.org/x/net/context"

	siteMb "bitbucket.org/canopei/mindbody/services/site"
	"bitbucket.org/canopei/site"
	"bitbucket.org/canopei/site/config"
	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	nsq "github.com/nsqio/go-nsq"
)

// Server is the account service implementation
type Server struct {
	MindbodyConfig *config.MindbodyConfig
	Logger         *logrus.Entry
	DB             *sqlx.DB
	SiteMBClient   *siteMb.Site_x0020_ServiceSoap
	Queue          *nsq.Producer
	SyncService    *site.SyncService
}

// NewServer creates a new Server instance
func NewServer(
	mindbodyConfig *config.MindbodyConfig,
	logger *logrus.Entry,
	db *sqlx.DB,
	siteMBClient *siteMb.Site_x0020_ServiceSoap,
	queue *nsq.Producer,
) *Server {
	return &Server{
		MindbodyConfig: mindbodyConfig,
		Logger:         logger,
		DB:             db,
		SiteMBClient:   siteMBClient,
		Queue:          queue,
	}
}

// Ping is used for healthchecks
func (s *Server) Ping(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	return &empty.Empty{}, nil
}
