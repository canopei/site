package main

import (
	"database/sql"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"golang.org/x/net/context"

	"encoding/json"

	"fmt"

	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	cStrings "bitbucket.org/canopei/golibs/strings"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	siteMb "bitbucket.org/canopei/mindbody/services/site"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// MaxSitesFetched is the limit of number of sites that can be fetched
	// at once.
	MaxSitesFetched int32 = 50
)

// CreateSite creates a Site
func (s *Server) CreateSite(ctx context.Context, req *sitePb.Site) (*sitePb.Site, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateSite: %v", req)
	defer timeTrack(logger, time.Now(), "CreateSite")

	// Validations
	trimmedName := strings.TrimSpace(req.Name)
	if trimmedName == "" && req.Name != "" {
		logger.Error("The site name cannot be whitespaces.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site name cannot be whitespaces.")
	}
	if req.MbId != 0 {
		rows, err := s.DB.Queryx(`SELECT site_id FROM site WHERE mb_id = ? AND deleted_at = '0'`, req.MbId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing site.")
		}
		if rows.Next() {
			logger.Errorf("A site already exists with this MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	// Create the site
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	if req.Name != "" {
		rand.Seed(time.Now().Unix())
		req.Slug = fmt.Sprintf("%s-%d", cStrings.Slugify(req.Name), rand.Intn(999)+1000)
	}

	_, err = s.DB.NamedExec(`INSERT INTO site
		(uuid, name, description, slug, contact_email, mb_id)
		VALUES (UNHEX(REPLACE(:uuid,'-','')), :name, :description, :slug, :contact_email, :mb_id)`, req)
	if err != nil {
		logger.Errorf("Cannot create database entry: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created site '%s'.", req.GetUuid())

	var site = &sitePb.Site{}
	err = s.DB.Get(site, "SELECT * FROM site WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		logger.Errorf("Error while fetching the new site: %v", err)
		return nil, err
	}
	site.Uuid = uuid.String()

	return site, nil
}

// GetSites fetches a list of sites
func (s *Server) GetSites(ctx context.Context, req *sitePb.GetSitesRequest) (*sitePb.SitesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSites: %v", req)
	defer timeTrack(logger, time.Now(), "GetSites")

	if req.Limit == 0 || req.Limit > MaxSitesFetched {
		req.Limit = MaxSitesFetched
	}

	logger.Debugf("Fetching %d sites, offset %d...", req.Limit, req.Offset)

	// Fetch the sites
	queryParams := map[string]interface{}{
		"limit":  req.Limit,
		"offset": req.Offset,
	}

	condition := ""
	if len(req.Uuids) > 0 {
		condition = " AND uuid IN ("
		for k, v := range req.Uuids {
			paramKey := "uuid" + strconv.Itoa(k)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ")"
	}

	if len(req.Ids) > 0 {
		condition = condition + " AND site_id IN ("
		for k, v := range req.Ids {
			paramKey := "id" + strconv.Itoa(k)
			condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ")"
	}

	rows, err := s.DB.NamedQuery(`SELECT * FROM site WHERE deleted_at = '0' `+condition+` ORDER BY site_id DESC LIMIT :limit OFFSET :offset`, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the sites")
	}

	sitesList := &sitePb.SitesList{}
	for rows.Next() {
		site := sitePb.Site{}
		err := rows.StructScan(&site)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the sites")
		}

		unpackedUUID, err := crypto.Parse([]byte(site.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		site.Uuid = unpackedUUID.String()

		sitesList.Sites = append(sitesList.Sites, &site)
	}

	return sitesList, nil
}

// GetSite retrieves and Site by UUID or ID
func (s *Server) GetSite(ctx context.Context, req *sitePb.GetSiteRequest) (*sitePb.Site, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSite: %v", req)
	defer timeTrack(logger, time.Now(), "GetSite")

	// Validations
	if req.Uuid == "" && req.Id == 0 {
		logger.Errorf("Either the site UUID or the ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Either the site UUID or the ID is required.")
	}

	var site sitePb.Site
	err := s.DB.Get(&site, `SELECT *
		FROM site
		WHERE (uuid = UNHEX(REPLACE(?,'-','')) OR site_id = ?)
			AND deleted_at = '0'`, req.Uuid, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find site '%s' (%d).", req.Uuid, req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching site")
	}

	unpackedUUID, err := crypto.Parse([]byte(site.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	site.Uuid = unpackedUUID.String()

	return &site, nil
}

// GetSiteByMBID fetches the Site with the given Mindbody ID
func (s *Server) GetSiteByMBID(ctx context.Context, req *sitePb.GetSiteRequest) (*sitePb.Site, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSiteByMBID: %v", req)
	defer timeTrack(logger, time.Now(), "GetSiteByMBID")

	// Validations
	if req.MbId == 0 {
		logger.Errorf("The site MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site MB ID is required.")
	}

	var site sitePb.Site
	err := s.DB.Get(&site, `SELECT *
		FROM site
		WHERE mb_id = ? AND deleted_at = '0'`, req.MbId)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find site with MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching site")
	}

	unpackedUUID, err := crypto.Parse([]byte(site.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	site.Uuid = unpackedUUID.String()

	return &site, nil
}

// GetSiteBySlug fetches the Site with the given slug
func (s *Server) GetSiteBySlug(ctx context.Context, req *sitePb.GetBySlugRequest) (*sitePb.Site, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSiteBySlug: %v", req)
	defer timeTrack(logger, time.Now(), "GetSiteBySlug")

	// Validations
	req.Slug = strings.TrimSpace(req.Slug)
	if req.Slug == "" {
		logger.Errorf("The slug is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The slug is required.")
	}

	var site sitePb.Site
	err := s.DB.Get(&site, `SELECT *
		FROM site
		WHERE slug = ? AND deleted_at = '0'`, req.Slug)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find site by slug '%s'.", req.Slug)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching site")
	}

	unpackedUUID, err := crypto.Parse([]byte(site.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	site.Uuid = unpackedUUID.String()

	return &site, nil
}

// GetSitesByMBIDs retrieves the Locations for a given list of Site MBIDs
func (s *Server) GetSitesByMBIDs(ctx context.Context, req *sitePb.GetSitesByMBIDsRequest) (*sitePb.SitesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSitesByMBIDs: %v", req)
	defer timeTrack(logger, time.Now(), "GetSitesByMBIDs")

	if req.MbIds == "" {
		logger.Error("At least one site MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one site MB ID is required.d")
	}

	mbIDs := map[int32]bool{}
	rawMBIDs := strings.Split(req.MbIds, ",")
	for _, v := range rawMBIDs {
		v = strings.TrimSpace(v)
		intV, err := strconv.ParseInt(v, 10, 32)
		if err == nil {
			mbIDs[int32(intV)] = true
		}
	}
	if len(mbIDs) == 0 {
		logger.Error("At least one site MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one site MB ID is required.")
	}

	// Fetch the locations
	queryParams := map[string]interface{}{}
	condition := " AND mb_id IN ("
	idx := 0
	for k := range mbIDs {
		paramKey := "id" + strconv.Itoa(idx)
		condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
		queryParams[paramKey] = k
		idx++
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	rows, err := s.DB.NamedQuery(`SELECT *
		FROM site
		WHERE deleted_at = '0'`+condition, queryParams)
	if err != nil {
		logger.Errorf("Error while fetching the locations: %v", err)
		return nil, err
	}

	sitesList := &sitePb.SitesList{}
	for rows.Next() {
		site := sitePb.Site{}
		err := rows.StructScan(&site)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the sites")
		}

		unpackedUUID, err := crypto.Parse([]byte(site.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		site.Uuid = unpackedUUID.String()

		sitesList.Sites = append(sitesList.Sites, &site)
	}

	return sitesList, nil
}

// GetSiteByLocationID retrieves the Site for a given Location DB ID
func (s *Server) GetSiteByLocationID(ctx context.Context, req *sitePb.GetSiteByLocationIDRequest) (*sitePb.Site, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSiteByLocationID: %v", req)
	defer timeTrack(logger, time.Now(), "GetSiteByLocationID")

	if req.LocationId < 1 {
		logger.Error("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}

	var site sitePb.Site
	err := s.DB.Get(&site, `SELECT s.*
		FROM site s
		INNER JOIN location l ON s.site_id = l.site_id
		WHERE l.location_id = ? AND l.deleted_at = '0' AND s.deleted_at = '0' LIMIT 1`, req.LocationId)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find site for location ID %d.", req.LocationId)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching site")
	}

	unpackedUUID, err := crypto.Parse([]byte(site.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	site.Uuid = unpackedUUID.String()

	return &site, nil
}

// GetSitesToSync fetches a list of sites that weren't synced for the given interval (seconds)
func (s *Server) GetSitesToSync(ctx context.Context, req *sitePb.GetSitesToSyncRequest) (*sitePb.SitesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSitesToSync: %v", req)
	defer timeTrack(logger, time.Now(), "GetSitesToSync")

	rows, err := s.DB.Queryx(`SELECT *
		FROM site
		WHERE deleted_at = '0'
			AND TIME_TO_SEC(TIMEDIFF(NOW(), synced_at)) >= ?
		ORDER BY site_id DESC`, req.SyncInterval)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the sites")
	}

	sitesList := &sitePb.SitesList{}
	for rows.Next() {
		site := sitePb.Site{}
		err := rows.StructScan(&site)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the sites")
		}

		unpackedUUID, err := crypto.Parse([]byte(site.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		site.Uuid = unpackedUUID.String()

		sitesList.Sites = append(sitesList.Sites, &site)
	}

	return sitesList, nil
}

// UpdateSite updates a Site
func (s *Server) UpdateSite(ctx context.Context, req *sitePb.Site) (*sitePb.Site, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateSite: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateSite")

	// Validations
	if req.Uuid == "" {
		logger.Error("The site UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site UUID is required.")
	}

	var site sitePb.Site
	err := s.DB.Get(&site, "SELECT * FROM site WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateSite - site not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - first select.")
	}
	// TODO: if we change the approved status, validate the ACL level
	if site.IsApproved != req.IsApproved {
		// check ACL
	}

	if req.Slug == "" {
		rand.Seed(time.Now().Unix())
		req.Slug = fmt.Sprintf("%s-%d", cStrings.Slugify(req.Name), rand.Intn(999)+1000)
	}

	_, err = s.DB.NamedExec(`UPDATE site SET
			description = :description, contact_email = :contact_email, sng_payment_method_mb_id = :sng_payment_method_mb_id,
			slug = CASE WHEN name <> '' THEN :slug ELSE '' END,
			name = :name, synced_at = :synced_at, is_approved = :is_approved
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	err = s.DB.Get(&site, "SELECT * FROM site WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateSite - site not found: %s", req.Uuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	site.Uuid = req.Uuid

	return &site, nil
}

// DeleteSite removes a Site (sets deleted_at)
func (s *Server) DeleteSite(ctx context.Context, req *sitePb.DeleteSiteRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteSite: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteSite")

	if req.Uuid == "" {
		logger.Error("The site UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site UUID is required.")
	}

	result, err := s.DB.Exec("UPDATE site SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteSite - site not found: %s", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetActivationCode creates a Site from Mindbody
func (s *Server) GetActivationCode(ctx context.Context, req *sitePb.GetSiteActivationCodeRequest) (*sitePb.GetSiteActivationCodeResponse, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetActivationCode: %v", req)
	defer timeTrack(logger, time.Now(), "GetActivationCode")

	// Validations
	if req.MbId == 0 {
		logger.Error("The site MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site MB ID is required.")
	}

	mbRequest := mbHelpers.CreateSiteMBRequest([]int32{req.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
	// Get activation code
	mbActivationCodeResponse, err := siteMBClient.GetActivationCode(&siteMb.GetActivationCode{Request: &siteMb.GetActivationCodeRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Mindbody API request error.")
	}
	mbActivationCodeResult := mbActivationCodeResponse.GetActivationCodeResult

	if mbActivationCodeResult.ErrorCode.Int == 900 {
		logger.Errorf("Mindbody API response error - site ID not found: %#v", mbActivationCodeResponse)
		return nil, grpc.Errorf(codes.NotFound, "Site MB ID %d was not found.", req.MbId)
	}

	if mbActivationCodeResult.ErrorCode.Int != 200 {
		logger.Errorf("Mindbody API response error: %#v", mbActivationCodeResponse)
		return nil, grpcUtils.InternalError(logger, nil, "Mindbody API response error: %s", mbActivationCodeResult.Message)
	}

	response := &sitePb.GetSiteActivationCodeResponse{
		MbId:           req.MbId,
		ActivationCode: mbActivationCodeResult.ActivationCode,
		ActivationLink: mbActivationCodeResult.ActivationLink,
	}

	return response, nil
}

// CreateSiteFromMB creates a Site from Mindbody
func (s *Server) CreateSiteFromMB(ctx context.Context, req *sitePb.CreateSiteFromMBRequest) (*sitePb.Site, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateSiteFromMB: %v", req)
	defer timeTrack(logger, time.Now(), "CreateSiteFromMB")

	if req.MbId == 0 {
		logger.Error("The site MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site MB ID is required.")
	}

	// Check that we don't already have this Site in DB
	_, err := s.GetSiteByMBID(ctx, &sitePb.GetSiteRequest{MbId: req.MbId})
	if err == nil {
		logger.Errorf("Site %d already exists.", req.MbId)
		return nil, grpc.Errorf(codes.AlreadyExists, "Site %d already exists.", req.MbId)
	}
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode != codes.NotFound {
			return nil, grpcUtils.InternalError(logger, err, "Failed to check if the sites already exists.")
		}
	}

	mbRequest := mbHelpers.CreateSiteMBRequest([]int32{req.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
	// Fetch the site from MB
	mbSitesResponse, err := siteMBClient.GetSites(&siteMb.GetSites{Request: &siteMb.GetSitesRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Mindbody API request error.")
	}
	mbSitesResult := mbSitesResponse.GetSitesResult
	if mbSitesResult.ErrorCode.Int != 200 {
		logger.Errorf("Mindbody API response error: %#v", mbSitesResponse)
		return nil, grpcUtils.InternalError(logger, nil, "Mindbody API response error: %s", mbSitesResult.Message)
	}

	if len(mbSitesResult.Sites.Site) == 0 {
		logger.Errorf("Source has no access to %d.", req.MbId)
		return nil, grpc.Errorf(codes.PermissionDenied, "Source has no access to %d.", req.MbId)
	}
	mbSite := mbSitesResult.Sites.Site[0]

	site := &sitePb.Site{
		Name:         strings.TrimSpace(mbSite.Name),
		Description:  strings.TrimSpace(mbSite.Description),
		ContactEmail: strings.TrimSpace(mbSite.ContactEmail),
		MbId:         mbSite.ID.Int,
	}
	newSite, err := s.CreateSite(ctx, site)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Failed to create site from MB site.")
	}

	// Sync the Site and locations right away
	go func() {
		s.SyncService.SyncSites([]string{newSite.Uuid})
	}()

	return newSite, nil
}

// SyncSiteWithMB queues the given Site to the Sync queue
func (s *Server) SyncSiteWithMB(ctx context.Context, req *sitePb.SyncSiteWithMBRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("SyncSiteWithMB: %v", req)
	defer timeTrack(logger, time.Now(), "SyncSiteWithMB")

	if len(req.Uuids) == 0 {
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one site UUID is required.")
	}

	go func() {
		for _, uuid := range req.Uuids {
			messageData := queueHelper.NewSyncSiteMessage(uuid)
			message, _ := json.Marshal(messageData)
			s.Queue.Publish(conf.Queue.SyncTopic, message)
		}
	}()

	return &empty.Empty{}, nil
}

// SyncSitesFromDBToES syncs the given sites by UUIDs from DB to ES
func (s *Server) SyncSitesFromDBToES(ctx context.Context, req *sitePb.SyncSitesFromDBToESRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("SyncSitesFromDBToES: %v", req)
	defer timeTrack(logger, time.Now(), "SyncSitesFromDBToES")

	req.Uuids = strings.TrimSpace(req.Uuids)
	if len(req.Uuids) > 0 {
		_, err := s.SyncService.SyncSitesFromDB(strings.Split(req.Uuids, ","))
		if err != nil {
			logger.Errorf("Error while syncing the sites: %v", err)
			return nil, grpcUtils.InternalError(logger, err, "Error while syncing the sites")
		}
	}

	return &empty.Empty{}, nil
}
