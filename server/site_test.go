package main

import (
	"context"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	siteColumns = []string{"site_id", "uuid", "name", "description", "contact_email", "mb_id", "sng_payment_method_mb_id", "is_approved",
		"synced_at", "created_at", "modified_at", "deleted_at"}
)

func TestCreateSiteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.Site
		ExpectedCode codes.Code
	}{
		{&sitePb.Site{Name: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.Site{}, codes.Unknown},
		{&sitePb.Site{Name: "foo"}, codes.Unknown},
		{&sitePb.Site{Name: "foo", MbId: 123}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		site, err := server.CreateSite(context.Background(), validationTest.Request)

		// we will always get nil
		assert.Nil(site, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateSiteSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Check for unique MBID
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sqlmock.NewRows([]string{"site_id"}))

	mock.ExpectExec("^INSERT INTO site").WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows(siteColumns).
		AddRow(1, "123456789012345", "name", "description", "foo@bar.com", 123, 456, 1, "2017-01-01", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(rows)

	site, err := server.CreateSite(context.Background(), &sitePb.Site{
		Name:                 "name",
		MbId:                 123,
		Description:          "description",
		ContactEmail:         "foo@bar.com",
		SngPaymentMethodMbId: 123,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(site)
	if site != nil {
		assert.NotEmpty(site.Uuid)
		assert.Equal("name", site.Name)
	}
}

func TestCreateSiteEmptySuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT INTO site").WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows(siteColumns).
		AddRow(1, "123456789012345", "", "", "", 0, 0, 1, "000-00-00", "2017-01-01", "2017-01-02", "0000-00-00")
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(rows)

	site, err := server.CreateSite(context.Background(), &sitePb.Site{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(site)
	if site != nil {
		assert.NotEmpty(site.Uuid)
		assert.Equal("", site.Name)
	}
}

func TestCreateSiteExistingMBID(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Check for unique MBID
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sqlmock.NewRows([]string{"site_id"}).AddRow(1))

	site, err := server.CreateSite(context.Background(), &sitePb.Site{
		Name:                 "name",
		MbId:                 123,
		Description:          "description",
		ContactEmail:         "foo@bar.com",
		SngPaymentMethodMbId: 123,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(site)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetSitesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	sites := []*sitePb.Site{
		&sitePb.Site{Id: 1, Uuid: "1234567890123456", Name: "name1", Description: "desc1", ContactEmail: "foo1@bar.com",
			MbId: -12, SngPaymentMethodMbId: 456, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
		&sitePb.Site{Id: 2, Uuid: "1234567890123456", Name: "name2", Description: "desc2", ContactEmail: "foo2@bar.com",
			MbId: 1, SngPaymentMethodMbId: 457, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
		&sitePb.Site{Id: 3, Uuid: "1234567890123456", Name: "name3", Description: "desc3", ContactEmail: "foo3@bar.com",
			MbId: 2, SngPaymentMethodMbId: 458, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
	}
	sitesRows := sqlmock.NewRows(siteColumns)
	for _, site := range sites {
		sitesRows.AddRow(site.Id, site.Uuid, site.Name, site.Description, site.ContactEmail, site.MbId, site.SngPaymentMethodMbId, 1,
			site.SyncedAt, site.CreatedAt, site.ModifiedAt, site.DeletedAt)
	}
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sitesRows)

	response, err := server.GetSites(context.Background(), &sitePb.GetSitesRequest{Uuids: []string{"uuid1", "uuid2"}, Limit: 10, Offset: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Sites, len(sites))
		assert.Equal("name2", response.Sites[1].Name)
	}
}

func TestGetSitesEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WithArgs("uuid1", "uuid2", 10, 15).WillReturnRows(sqlmock.NewRows(siteColumns))

	response, err := server.GetSites(context.Background(), &sitePb.GetSitesRequest{Uuids: []string{"uuid1", "uuid2"}, Limit: 10, Offset: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Sites, 0)
	}
}

func TestGetSiteByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetSiteRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetSiteRequest{}, codes.InvalidArgument},
		{&sitePb.GetSiteRequest{MbId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetSiteRequest{MbId: 1}, codes.Unknown},
		{&sitePb.GetSiteRequest{MbId: -1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		site, err := server.GetSiteByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(site, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSiteByMBIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WithArgs(1).WillReturnRows(sqlmock.NewRows(siteColumns).
		AddRow(1, "1234567890123456", "name", "description", "foo@bar.com", 123, 456, 1, "2017-01-01", "2017-01-01", "2017-01-02", "2017-01-03"))

	site, err := server.GetSiteByMBID(context.Background(), &sitePb.GetSiteRequest{MbId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(site)
	if site != nil {
		assert.Equal("foo@bar.com", site.ContactEmail)
	}
}

func TestGetSiteByMBIDNotFound(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WithArgs(1).WillReturnRows(sqlmock.NewRows(siteColumns))

	site, err := server.GetSiteByMBID(context.Background(), &sitePb.GetSiteRequest{MbId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(site)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetSiteBySlugValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetBySlugRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetBySlugRequest{}, codes.InvalidArgument},
		{&sitePb.GetBySlugRequest{Slug: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetBySlugRequest{Slug: "foo-bar "}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		site, err := server.GetSiteBySlug(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(site, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSiteBySlugSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WithArgs("foo-bar").WillReturnRows(sqlmock.NewRows(siteColumns).
		AddRow(1, "1234567890123456", "name", "description", "foo@bar.com", 123, 456, 1, "2017-01-01", "2017-01-01", "2017-01-02", "2017-01-03"))

	site, err := server.GetSiteBySlug(context.Background(), &sitePb.GetBySlugRequest{Slug: "foo-bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(site)
	if site != nil {
		assert.Equal("foo@bar.com", site.ContactEmail)
	}
}

func TestGetSiteBySlugNotFound(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WithArgs("foo-bar").WillReturnRows(sqlmock.NewRows(siteColumns))

	site, err := server.GetSiteBySlug(context.Background(), &sitePb.GetBySlugRequest{Slug: "foo-bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(site)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetSiteByMBIDsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetSitesByMBIDsRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetSitesByMBIDsRequest{}, codes.InvalidArgument},
		{&sitePb.GetSitesByMBIDsRequest{MbIds: ""}, codes.InvalidArgument},
		{&sitePb.GetSitesByMBIDsRequest{MbIds: " ,"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetSitesByMBIDsRequest{MbIds: "123"}, codes.Unknown},
		{&sitePb.GetSitesByMBIDsRequest{MbIds: ",4566,123 ,"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		site, err := server.GetSitesByMBIDs(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(site, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSiteByMBIDsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	sites := []*sitePb.Site{
		&sitePb.Site{Id: 1, Uuid: "1234567890123456", Name: "name1", Description: "desc1", ContactEmail: "foo1@bar.com",
			MbId: -12, SngPaymentMethodMbId: 456, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
		&sitePb.Site{Id: 2, Uuid: "1234567890123456", Name: "name2", Description: "desc2", ContactEmail: "foo2@bar.com",
			MbId: 1, SngPaymentMethodMbId: 457, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
		&sitePb.Site{Id: 3, Uuid: "1234567890123456", Name: "name3", Description: "desc3", ContactEmail: "foo3@bar.com",
			MbId: 2, SngPaymentMethodMbId: 458, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
	}
	sitesRows := sqlmock.NewRows(siteColumns)
	for _, site := range sites {
		sitesRows.AddRow(site.Id, site.Uuid, site.Name, site.Description, site.ContactEmail, site.MbId, site.SngPaymentMethodMbId,
			1, site.SyncedAt, site.CreatedAt, site.ModifiedAt, site.DeletedAt)
	}

	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sitesRows)

	response, err := server.GetSitesByMBIDs(context.Background(), &sitePb.GetSitesByMBIDsRequest{MbIds: ",123,456,"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Sites, len(sites))
		assert.Equal("foo2@bar.com", response.Sites[1].ContactEmail)
	}
}

func TestGetSiteByMBIDsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sqlmock.NewRows(siteColumns))

	response, err := server.GetSitesByMBIDs(context.Background(), &sitePb.GetSitesByMBIDsRequest{MbIds: ",123,456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Sites, 0)
	}
}

func TestGetSiteByLocationIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.GetSiteByLocationIDRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.GetSiteByLocationIDRequest{}, codes.InvalidArgument},
		{&sitePb.GetSiteByLocationIDRequest{LocationId: -2}, codes.InvalidArgument},
		{&sitePb.GetSiteByLocationIDRequest{LocationId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.GetSiteByLocationIDRequest{LocationId: 123}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.GetSiteByLocationID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSiteByLocationIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WithArgs(123).WillReturnRows(sqlmock.NewRows(siteColumns).
		AddRow(1, "1234567890123456", "name", "description", "foo@bar.com", 123, 456, 1, "2017-01-01", "2017-01-01", "2017-01-02", "2017-01-03"))

	site, err := server.GetSiteByLocationID(context.Background(), &sitePb.GetSiteByLocationIDRequest{LocationId: 123})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(site)
	if site != nil {
		assert.Equal("foo@bar.com", site.ContactEmail)
	}
}

func TestGetSiteByLocationIDNotFound(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WithArgs(123).WillReturnRows(sqlmock.NewRows(siteColumns))

	site, err := server.GetSiteByLocationID(context.Background(), &sitePb.GetSiteByLocationIDRequest{LocationId: 123})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(site)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestDeleteSiteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.DeleteSiteRequest
		ExpectedCode codes.Code
	}{
		{&sitePb.DeleteSiteRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.DeleteSiteRequest{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.DeleteSite(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteSite(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE site SET(.+)deleted_at").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteSite(context.Background(), &sitePb.DeleteSiteRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE site SET(.+)deleted_at").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteSite(context.Background(), &sitePb.DeleteSiteRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestUpdateSiteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *sitePb.Site
		ExpectedCode codes.Code
	}{
		{&sitePb.Site{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&sitePb.Site{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		location, err := server.UpdateSite(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(location, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateSiteMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sqlmock.NewRows(siteColumns))

	site, err := server.UpdateSite(context.Background(), &sitePb.Site{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(site)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateSiteSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testSite := &sitePb.Site{
		Uuid:         "123456789012345",
		MbId:         123,
		Name:         "name",
		ContactEmail: "foo@bar.com",
	}

	// The check that it exists
	rows := sqlmock.NewRows(siteColumns).
		AddRow(1, "123456789012345", "name", "description", "foo@bar.com", 123, 456, 1, "2017-01-01", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(rows)

	mock.ExpectExec("^UPDATE site SET").WillReturnResult(sqlmock.NewResult(0, 1))

	rows = sqlmock.NewRows(siteColumns).
		AddRow(1, "123456789012345", "name", "description", "foo@bar.com", 123, 456, 1, "2017-01-01", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(rows)

	site, err := server.UpdateSite(context.Background(), testSite)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(site)
}

func TestGetSitesToSyncSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	sites := []*sitePb.Site{
		&sitePb.Site{Id: 1, Uuid: "1234567890123456", Name: "name1", Description: "desc1", ContactEmail: "foo1@bar.com",
			MbId: -12, SngPaymentMethodMbId: 456, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
		&sitePb.Site{Id: 2, Uuid: "1234567890123456", Name: "name2", Description: "desc2", ContactEmail: "foo2@bar.com",
			MbId: 1, SngPaymentMethodMbId: 457, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
		&sitePb.Site{Id: 3, Uuid: "1234567890123456", Name: "name3", Description: "desc3", ContactEmail: "foo3@bar.com",
			MbId: 2, SngPaymentMethodMbId: 458, SyncedAt: "2017-01-01", CreatedAt: "2017-01-01", ModifiedAt: "2017-01-02", DeletedAt: "2017-01-03"},
	}
	sitesRows := sqlmock.NewRows(siteColumns)
	for _, site := range sites {
		sitesRows.AddRow(site.Id, site.Uuid, site.Name, site.Description, site.ContactEmail, site.MbId, site.SngPaymentMethodMbId,
			1, site.SyncedAt, site.CreatedAt, site.ModifiedAt, site.DeletedAt)
	}
	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sitesRows)

	response, err := server.GetSitesToSync(context.Background(), &sitePb.GetSitesToSyncRequest{SyncInterval: 100})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Sites, len(sites))
		assert.Equal("name2", response.Sites[1].Name)
	}
}

func TestGetSitesToSyncEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM site").WillReturnRows(sqlmock.NewRows(siteColumns))

	response, err := server.GetSitesToSync(context.Background(), &sitePb.GetSitesToSyncRequest{SyncInterval: 100})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Sites, 0)
	}
}
