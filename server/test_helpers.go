package main

import (
	"io/ioutil"

	siteMb "bitbucket.org/canopei/mindbody/services/site"
	"bitbucket.org/canopei/site/config"
	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	nsq "github.com/nsqio/go-nsq"
)

// NewServerMock creates a mock of Server
func NewServerMock(db *sqlx.DB) *Server {
	mindbodyConfig := &config.MindbodyConfig{
		SourceName:     "foo",
		SourcePassword: "bar",
	}

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	siteMBClient := siteMb.NewSite_x0020_ServiceSoap("", false, nil)

	queueProducer := &nsq.Producer{}

	return NewServer(mindbodyConfig, logger, db, siteMBClient, queueProducer)
}
