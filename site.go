package site

import (
	sitePb "bitbucket.org/canopei/site/protobuf"
)

// ESSite is the ES representation of the Site type
type ESSite struct {
	UUID string `json:"uuid"`
}

// GetESSite creates a ESSite from a Site object
func GetESSite(site *sitePb.Site) *ESSite {
	return &ESSite{
		UUID: site.Uuid,
	}
}
