package site

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/slices"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	siteMb "bitbucket.org/canopei/mindbody/services/site"
	"bitbucket.org/canopei/site/config"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"

	queueHelper "bitbucket.org/canopei/golibs/queue"
)

var (
	// SNGCustomPaymentMethodName is the name of the custom payment method that will be used by SNG
	SNGCustomPaymentMethodNames = []string{"sweatnglow", "sweat n glow"}
)

// SyncService is the implementation of a sync service
type SyncService struct {
	Logger               *logrus.Entry
	SiteMBClient         *siteMb.Site_x0020_ServiceSoap
	SaleMBClient         *saleMb.Sale_x0020_ServiceSoap
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	SiteService          sitePb.SiteServiceClient
	MindbodyConfig       *config.MindbodyConfig
}

// NewSyncService creates a new SyncService instance
func NewSyncService(
	logger *logrus.Entry,
	siteMBClient *siteMb.Site_x0020_ServiceSoap,
	saleMBClient *saleMb.Sale_x0020_ServiceSoap,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
	siteService sitePb.SiteServiceClient,
	mindbodyConfig *config.MindbodyConfig,
) *SyncService {
	return &SyncService{
		Logger:               logger,
		SiteMBClient:         siteMBClient,
		SaleMBClient:         saleMBClient,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		SiteService:          siteService,
		MindbodyConfig:       mindbodyConfig,
	}
}

// SyncSites synchronizes the given sites (UUIDs) to DB and ES from MB
func (ss *SyncService) SyncSites(siteUUIDs []string) ([]string, error) {
	var messagesToFinish []string
	var dbSites []*sitePb.Site
	var err error

	// Get the sites from DB
	sites, err := ss.SiteService.GetSites(context.Background(), &sitePb.GetSitesRequest{Uuids: siteUUIDs})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch sites by UUIDs: %v", err)
		return nil, fmt.Errorf("Cannot fetch sites by UUIDs: %v", err)
	}

	if len(sites.Sites) == 0 {
		// Blindly attempt to remove them from ES
		for _, siteUUID := range siteUUIDs {
			deleteSiteFromES(ss.ElasticsearchService, "sng", siteUUID)
		}

		return nil, fmt.Errorf("No website was found in DB")
	}

	dbSites = sites.Sites

	ss.Logger.Debugf("Syncing %d sites. Requested %d sites.", len(dbSites), len(siteUUIDs))

	if len(dbSites) < len(siteUUIDs) {
		ss.Logger.Debugf("Removing from ES some sites...")
		for _, siteUUID := range siteUUIDs {
			for _, dbSite := range dbSites {
				if dbSite.Uuid == siteUUID {
					goto OUTER_SITE
				}
			}

			ss.Logger.Debugf("Deleting from ES site '%s'", siteUUID)
			_, err = deleteSiteFromES(ss.ElasticsearchService, "sng", siteUUID)
			if err != nil {
				ss.Logger.Errorf("Failed to remove from ES site '%s'", siteUUID)
			}
		OUTER_SITE:
		}
	}

	sitesMBIDs := []int32{}
	for _, dbSite := range dbSites {
		sitesMBIDs = append(sitesMBIDs, dbSite.MbId)
	}
	mbRequest := mbHelpers.CreateSiteMBRequest(sitesMBIDs, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)
	// Fetch the sites from Mindbody
	mbSitesResponse, err := ss.SiteMBClient.GetSites(&siteMb.GetSites{Request: &siteMb.GetSitesRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the sites from Mindbody: %v", err)
		return nil, fmt.Errorf("Cannot fetch the sites from Mindbody: %v", err)
	}
	mbSitesResult := mbSitesResponse.GetSitesResult
	if mbSitesResult.ErrorCode.Int != 200 {
		ss.Logger.Errorf("Mindbody API response error: %s", mbSitesResult.Message)
		return nil, fmt.Errorf("Mindbody API response error: %s", mbSitesResult.Message)
	}
	ss.Logger.Debugf("Found %d sites in Mindbody.", len(mbSitesResult.Sites.Site))
	if len(mbSitesResult.Sites.Site) == 0 {
		ss.Logger.Errorf("Source has no access to any of the sites.")
		return nil, fmt.Errorf("Source has no access to any of the sites")
	}
	// TODO: handle here the case were a studio removes the access for our creds.

	for _, mbResSite := range mbSitesResult.Sites.Site {
		// Check for the custom payment method
		SNGPaymentMethodID, err := ss.GetSNGPaymentMethodIDForSiteMBID(mbResSite.ID.Int)
		if err != nil {
			SNGPaymentMethodID = 0
		}

		dbSite, err := findSiteByMBID(dbSites, mbResSite.ID.Int)
		if err != nil {
			ss.Logger.Errorf("DB site not found after MB fetch (MB ID %d).", mbResSite.ID)
			continue
		}
		ss.Logger.Debugf("Found site with MB ID %d in DB: %#v", mbResSite.ID.Int, dbSite)

		// Do not start site syncs faster than 5 minute before the last sync!
		lastSyncTime, err := time.Parse(sqlDatetimeFormat, dbSite.SyncedAt)
		duration := time.Since(lastSyncTime)
		if duration.Seconds() <= 60 {
			ss.Logger.Infof("DB site MB ID %d sync too fast (5 mins) (last was at %s).", mbResSite.ID.Int, dbSite.SyncedAt)
			messagesToFinish = append(messagesToFinish, dbSite.Uuid)
			continue
		}

		mbSite := dbSite
		mbSite.SngPaymentMethodMbId = SNGPaymentMethodID
		mbSite.SyncedAt = time.Now().Format(sqlDatetimeFormat)
		// This is the case for first import; get the initial data from MB
		if dbSite.Name == "" {
			mbSite = MbSiteToPbSite(mbResSite)
			mbSite.Uuid = dbSite.Uuid
			mbSite.SngPaymentMethodMbId = SNGPaymentMethodID
			mbSite.SyncedAt = time.Now().Format(sqlDatetimeFormat)
		}
		dbSite, err = ss.SiteService.UpdateSite(context.Background(), mbSite)
		if err != nil {
			ss.Logger.Errorf("Failed to update site '%s' in DB: %v", dbSite.Uuid, err)
			continue
		}

		if !dbSite.IsApproved {
			ss.Logger.Debugf("Site with MB ID %d is not approved for publishing.", dbSite.MbId)
		} else {
			ss.Logger.Infof("Publishing site MB ID %d to ES.", dbSite.MbId)
			_, err = indexSiteToES(ss.ElasticsearchService, "sng", dbSite)
			if err != nil {
				ss.Logger.Errorf("Failed to update site '%s' in ES: %v", dbSite.Uuid, err)
				continue
			}
		}

		// Add to the queue a sync for Locations message
		messageData := queueHelper.NewSyncSiteLocationsMessage(dbSite.MbId)
		message, _ := json.Marshal(messageData)
		ss.Queue.Publish("sync", message)

		// Add to the queue a sync for Staff message
		staffMessageData := queueHelper.NewSyncSitesStaffMessage(dbSite.Uuid)
		staffMessage, _ := json.Marshal(staffMessageData)
		ss.Queue.Publish("sync", staffMessage)

		// Add to the queue a sync for Site Clients
		clientsMessageData := queueHelper.NewSyncSitesClientsMessage(dbSite.Uuid)
		clientsMessage, _ := json.Marshal(clientsMessageData)
		ss.Queue.Publish("sync", clientsMessage)

		// Add to the queue a sync for Site Class Schedules
		classSchedulesMessageData := queueHelper.NewSyncSiteClassSchedules(dbSite.Uuid)
		classSchedulesMessage, _ := json.Marshal(classSchedulesMessageData)
		ss.Queue.Publish("sync", classSchedulesMessage)

		messagesToFinish = append(messagesToFinish, dbSite.Uuid)
	}

	return messagesToFinish, nil
}

// SyncSiteLocations synchronizes the given sites (UUIDs) locations to DB and ES from MB
func (ss *SyncService) SyncSiteLocations(siteMBIDs []int32) ([]string, error) {
	var messagesToFinish []string

	ss.Logger.Infof("SyncSiteLocations with %v", siteMBIDs)

	// Get the locations from MB
	mbRequest := mbHelpers.CreateSiteMBRequest(siteMBIDs, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)
	// Fetch the locations from Mindbody
	mbLocationsResponse, err := ss.SiteMBClient.GetLocations(&siteMb.GetLocations{Request: &siteMb.GetLocationsRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the locations from Mindbody: %v", err)
		return nil, fmt.Errorf("Cannot fetch the locations from Mindbody: %v", err)
	}
	mbLocationsResult := mbLocationsResponse.GetLocationsResult
	if mbLocationsResult.ErrorCode.Int != 200 {
		ss.Logger.Errorf("Mindbody API response error: %s", mbLocationsResult.Message)
		return nil, fmt.Errorf("Mindbody API response error: %s", mbLocationsResult.Message)
	}

	ss.Logger.Infof("Found %d locations in Mindbody.", len(mbLocationsResult.Locations.Location))
	if len(mbLocationsResult.Locations.Location) == 0 {
		ss.Logger.Info("No locations were found.")
	}

	// Group by site MB ID
	mbLocationsBySiteMBID := map[int32][]*siteMb.Location{}
	for _, v := range mbLocationsResult.Locations.Location {
		mbLocationsBySiteMBID[v.SiteID.Int] = append(mbLocationsBySiteMBID[v.SiteID.Int], v)
	}

	// Fetch the locations from DB
	siteMBIDsToFetch := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(siteMBIDs)), ","), "[]")
	locations, err := ss.SiteService.GetSitesLocationsByMBID(context.Background(), &sitePb.GetSitesLocationsByMBIDRequest{MbIds: siteMBIDsToFetch})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch locations by sites MBIDs: %v", err)
		return nil, fmt.Errorf("Cannot fetch locations by sites MBIDs: %v", err)
	}

	if len(locations.Locations) == 0 {
		ss.Logger.Infof("No location was found in DB.")
	} else {
		ss.Logger.Infof("%d locations were found in DB.", len(locations.Locations))
	}
	dbLocations := map[int32]*sitePb.Location{}
	for _, v := range locations.Locations {
		dbLocations[v.MbId] = v
	}

	// Fetch the sites from DB - we need the site_id :/
	sites, err := ss.SiteService.GetSitesByMBIDs(context.Background(), &sitePb.GetSitesByMBIDsRequest{MbIds: siteMBIDsToFetch})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the sites from DB - MBIDs: %v", err)
		return nil, fmt.Errorf("Cannot fetch the sites from DB - MBIDs: %v", err)
	}
	if len(sites.Sites) == 0 {
		ss.Logger.Errorf("No sites were found in DB. Cannot continue...")
		return nil, fmt.Errorf("No sites were found in DB. Cannot continue")
	}
	dbSites := sites.Sites

	// Go through each site MB ID
OUTER:
	for siteMBID, mbLocations := range mbLocationsBySiteMBID {
		mbLocationsMBIDs := map[int32]bool{}

		dbSite, _ := findSiteByMBID(dbSites, siteMBID)
		for _, mbLocation := range mbLocations {
			mbLocationsMBIDs[mbLocation.ID.Int] = true

			ss.Logger.Infof("Syncing location MB ID %d for site ID %d.", mbLocation.ID.Int, dbSite.Id)

			dbLocation, ok := dbLocations[mbLocation.ID.Int]
			if !ok {
				ss.Logger.Infof("Adding new location %d in DB.", mbLocation.ID.Int)

				// New Location, add to DB
				newLocation := mbLocationToPbLocation(mbLocation)
				newLocation.SiteId = dbSite.Id

				dbLocation, err = ss.SiteService.CreateLocation(context.Background(), newLocation)
				if err != nil {
					ss.Logger.Errorf("Cannot create location in DB: %v", err)
					continue OUTER
				}
			} else {
				ss.Logger.Infof("Existing location %d in DB.", mbLocation.ID.Int)

				currentMbLocation := mbLocationToPbLocation(mbLocation)

				// Check if we need to update
				if hasLocationChanged(dbLocation, currentMbLocation) {
					ss.Logger.Infof("Updating location %d (%s) in DB.", mbLocation.ID.Int, dbSite.Uuid)

					changedLocation := dbLocation
					changedLocation.HasClasses = currentMbLocation.HasClasses

					dbLocation, err = ss.SiteService.UpdateLocation(context.Background(), changedLocation)
					if err != nil {
						ss.Logger.Errorf("Cannot update location '%s' in DB: %v", changedLocation.Uuid, err)
						continue OUTER
					}
				}
			}

			// we will PUT this in Elasticsearch
			if dbLocation.IsApproved {
				ss.Logger.Infof("Publishing location MB ID (%d) to ES.", dbLocation.MbId, dbSite.Id)
				_, err = indexLocationToES(ss.ElasticsearchService, "sng", dbSite.Uuid, dbLocation)
				if err != nil {
					ss.Logger.Errorf("Failed to update location '%s' in ES: %v", dbSite.Uuid, err)
					continue OUTER
				}
			} else {
				ss.Logger.Infof("Skipping ES publish. Location MB ID %d (%d) is not approved for publishing.", dbLocation.MbId, dbSite.Id)
			}
		}

		// Delete untouched locations
		for mbID, mbLocation := range dbLocations {
			if _, ok := mbLocationsMBIDs[mbID]; !ok {
				ss.Logger.Infof("Deleting untouched location UUID: %s", mbLocation.Uuid)

				_, err = deleteLocationFromES(ss.ElasticsearchService, "sng", dbSite.Uuid, mbLocation.Uuid)
				if err != nil {
					ss.Logger.Errorf("Failed to delete location '%s' from ES: %v", mbLocation.Uuid, err)
				}

				_, err := ss.SiteService.DeleteLocation(context.Background(), &sitePb.DeleteLocationRequest{Uuid: mbLocation.Uuid})
				if err != nil {
					ss.Logger.Errorf("Cannot delete location '%s' from DB: %v", mbLocation.Uuid, err)
					continue
				}
			}
		}

		messagesToFinish = append(messagesToFinish, strconv.FormatInt(int64(siteMBID), 10))
	}

	return messagesToFinish, nil
}

// SyncSitesFromDB synchronizes the given sites (UUIDs) to ES from DB
func (ss *SyncService) SyncSitesFromDB(siteUUIDs []string) ([]string, error) {
	var messagesToFinish []string
	var dbSites []*sitePb.Site
	var err error

	// Get the locations from DB
	sites, err := ss.SiteService.GetSites(context.Background(), &sitePb.GetSitesRequest{Uuids: siteUUIDs})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch sites by UUIDs: %v", err)
		return nil, fmt.Errorf("Cannot fetch sites by UUIDs: %v", err)
	}
	dbSites = sites.Sites

	ss.Logger.Debugf("Syncing %d sites from DB. Requested %d sites.", len(dbSites), len(siteUUIDs))

	for _, dbSite := range dbSites {
		messagesToFinish = append(messagesToFinish, dbSite.Uuid)

		if dbSite.IsApproved == false {
			ss.Logger.Infof("Changes to site MB ID %d not approved for publishing.", dbSite.MbId)
			continue
		}

		_, err = indexSiteToES(ss.ElasticsearchService, "sng", dbSite)
		if err != nil {
			ss.Logger.Errorf("Failed to update site '%s' in ES: %v", dbSite.Uuid, err)
			continue
		}
	}

	return messagesToFinish, nil
}

// SyncLocationsFromDB synchronizes the given locations (UUIDs) to ES from DB
func (ss *SyncService) SyncLocationsFromDB(locationUUIDs []string) ([]string, error) {
	var messagesToFinish []string
	var dbLocations []*sitePb.Location
	var err error

	// Get the locations from DB
	locations, err := ss.SiteService.GetLocations(context.Background(), &sitePb.GetLocationsRequest{Uuids: strings.Join(locationUUIDs, ",")})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch locations by UUIDs: %v", err)
		return nil, fmt.Errorf("Cannot fetch locations by UUIDs: %v", err)
	}
	dbLocations = locations.Locations

	ss.Logger.Debugf("Syncing %d locations from DB. Requested %d locations.", len(dbLocations), len(locationUUIDs))

	for _, dbLocation := range dbLocations {
		messagesToFinish = append(messagesToFinish, dbLocation.Uuid)

		if dbLocation.IsApproved == false {
			ss.Logger.Infof("Changes to location MB ID %d not approved for publishing.", dbLocation.MbId)
			continue
		}

		// get the parent Site
		dbSite, err := ss.SiteService.GetSite(context.Background(), &sitePb.GetSiteRequest{Id: dbLocation.SiteId})
		if err != nil {
			ss.Logger.Errorf("Failed to fetch location's '%s' site from DB: %v", dbLocation.Uuid, err)
			continue
		}

		_, err = indexLocationToES(ss.ElasticsearchService, "sng", dbSite.Uuid, dbLocation)
		if err != nil {
			ss.Logger.Errorf("Failed to update location '%s' in ES: %v", dbLocation.Uuid, err)
			continue
		}
	}

	return messagesToFinish, nil
}

// GetSNGPaymentMethodIDForSiteMBID synchronizes the given site class sales to DB and ES from MB
func (ss *SyncService) GetSNGPaymentMethodIDForSiteMBID(siteMBID int32) (int32, error) {
	ss.Logger.Infof("GetSNGPaymentMethodIDForSiteMBID %d", siteMBID)

	sngMethodNames := SNGCustomPaymentMethodNames
	// For -99, the sandbox, we can't add custom payment, so we'll use the paypal one
	if siteMBID == -99 {
		sngMethodNames = []string{"paypal"}
	}

	// Get the custom payment methods
	mbRequest := mbHelpers.CreateSaleMBRequest([]int32{siteMBID}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)
	// Fetch the methods from Mindbody
	mbPMsResponse, err := ss.SaleMBClient.GetCustomPaymentMethods(&saleMb.GetCustomPaymentMethods{Request: &saleMb.GetCustomPaymentMethodsRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		return 0, fmt.Errorf("Cannot fetch custom payment methods from Mindbody: %v", err)
	}
	mbPMsResult := mbPMsResponse.GetCustomPaymentMethodsResult
	if mbPMsResult.ErrorCode.Int != 200 {
		return 0, fmt.Errorf("Mindbody API response error: %s", mbPMsResult.Message)
	}

	ss.Logger.Infof("Found %d custom payment methods in Mindbody.", len(mbPMsResult.PaymentMethods.CustomPaymentInfo))
	if len(mbPMsResult.PaymentMethods.CustomPaymentInfo) == 0 {
		return 0, fmt.Errorf("No custom payment methods were found")
	}

	var customPMs []string
	var paymentMethodID int32
	var paymentMethodName string
	for _, cpm := range mbPMsResult.PaymentMethods.CustomPaymentInfo {
		customPMs = append(customPMs, cpm.PaymentInfo.Name)

		paymentMethodName = strings.ToLower(strings.TrimSpace(cpm.PaymentInfo.Name))
		if slices.Scontains(sngMethodNames, paymentMethodName) {
			paymentMethodID = cpm.ID.Int
			break
		}
	}
	if paymentMethodID == 0 {
		ss.Logger.Infof("Custom payment methods found %d: [%s]", siteMBID, strings.Join(customPMs, ","))

		return 0, fmt.Errorf("SNG custom payment method (%s) not found for site MB ID %d", strings.Join(sngMethodNames, ", "), siteMBID)
	}

	return paymentMethodID, nil
}
